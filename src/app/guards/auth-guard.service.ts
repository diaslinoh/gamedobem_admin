import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserService } from '../services/user.service';
import { UtilsService } from '../services/utils.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  private isAuthenticated: boolean = false;

  constructor(
    private userService: UserService,
    private sUtils: UtilsService,
  ) { }


  canActivate() {
    if (this.userService.checkUserStatus())
      this.isAuthenticated = true;
    else {
      this.userService.destroyUser();
      this.sUtils.goUrl();
    }
    return this.isAuthenticated;
  }
}
