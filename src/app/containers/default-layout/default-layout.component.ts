import { Component, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { ApiService } from '../../services/api.service';
import { UserService } from '../../services/user.service';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-dashboard',
  styles: [""],
  templateUrl: './default-layout.component.html'

})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(
    private sApi: ApiService,
    private sUser: UserService,
    private sUtils: UtilsService,
  ) {

  }

  ngOnInit() {
  }
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  logout() {
    this.sUser.destroyUser();
    this.sUtils.goUrl();
  }
}
