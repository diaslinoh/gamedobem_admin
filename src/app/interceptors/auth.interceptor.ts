import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { UserService } from '../services/user.service';
import { tap } from 'rxjs/operators';
import { UtilsService } from '../services/utils.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private userService: UserService,
    private sUtils: UtilsService,
  ) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log("INRERCEPTOR", request);
    let dupReq = request.clone();
    dupReq.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    if (this.userService.checkUserStatus()) {
      dupReq = dupReq.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + this.userService.getToken()),
      });
    }
    return next.handle(dupReq).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // console.log(event);
        if (event && event['status'] == 401) {
          this.userService.destroyUser();
          this.sUtils.goUrl();
        }
      }

    }));
  }
}
