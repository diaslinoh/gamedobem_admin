import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { UserService } from '../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';


@Injectable()
export class Commonterceptor implements HttpInterceptor {
  constructor(
    private spinner: NgxSpinnerService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log("COMMON INTERCEPTOR");

    // console.log(request);
    if (!request['reportProgress'])
      this.spinner.show();
    return next.handle(request).pipe(finalize(() => {
      if (!request['reportProgress'])
        this.spinner.hide();
    }));
  }
}
