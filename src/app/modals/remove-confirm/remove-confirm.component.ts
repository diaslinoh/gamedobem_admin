import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DictionaryService } from '../../services/dictionary.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-remove-confirm',
  templateUrl: './remove-confirm.component.html',
  styleUrls: ['./remove-confirm.component.scss']
})
export class RemoveConfirmComponent implements OnInit {
  public onConfirm: Subject<any>;
  constructor(
    private sDic: DictionaryService,
    private modalService: BsModalService,
    private bsModalRef: BsModalRef,
  ) { }

  ngOnInit() {
    this.onConfirm = new Subject();
  }

  confirm() {
    this.onConfirm.next("");
  }
}
