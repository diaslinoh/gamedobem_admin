import {
    animation, trigger, animateChild, group,
    transition, animate, style, query
} from '@angular/animations';

export const opacity =
    trigger(
        'opacity', [
        transition(':enter', [
            style({ opacity: 0 }),
            animate('500ms', style({ opacity: 1 }))
        ]),
        transition(':leave', [
            style({ opacity: 1 }),
            animate('500ms', style({ opacity: 0 }))
        ])
    ]
    );