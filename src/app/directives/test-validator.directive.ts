import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[isMin]',
  providers: [{provide: NG_VALIDATORS, useExisting: TestValidatorDirective, multi: true}]
})
export class TestValidatorDirective {
@Input() isMin=null ;
  constructor() { }
    public validate(control: AbstractControl, val): {[key: string]: any} {
      // console.log(this.isMin);
      let tmp = parseInt(control['value']) >= 1;
      return tmp? null:{min:this.isMin};
  }


}

// @Directive({
//   selector: '[isMin]',
//   providers: [{provide: NG_VALIDATORS, useExisting: TestValidatorDirective, multi: true}]
// })
// export class TestValidatorDirective implements Validator {

//   public constructor() {}

//   public validate(control: AbstractControl): {[key: string]: any} {
//       console.log(control, "VALIDATION");
      
//       return null;
//   }

// }