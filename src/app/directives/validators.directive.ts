import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';


@Directive({
  selector: '[min]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true }]
})
export class MinValidatorDirective {

  @Input() min = null;
  constructor() { }
  public validate(control: AbstractControl, val): { [key: string]: any } {
    let tmp = parseInt(control['value']) >= this.min;
    return tmp ? null : { min: this.min };
  }

}
@Directive({
  selector: '[max]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true }]
})
export class MaxValidatorDirective {

  @Input() max = null;
  constructor() { }
  public validate(control: AbstractControl, val): { [key: string]: any } {
    let tmp = parseInt(control['value']) <= this.max;
    return tmp ? null : { max: this.max };
  }

}


