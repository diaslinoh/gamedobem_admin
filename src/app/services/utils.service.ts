import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DictionaryService } from './dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private sDic: DictionaryService,
  ) { }

  goUrl(url = "/login") {
    this.router.navigate([url]);
  }

  mapObject(data = {}) {
    try {
      let tmpData = Object.assign({}, data);
      Object.keys(tmpData).forEach(el => {
        if (tmpData[el] == null)
          delete tmpData[el];
      });
      return tmpData;
    } catch (error) {
      return {};
    }
  }

  isDate(date = "", split = true) {

    try {
      let tmpDate;
      if (split) {
        tmpDate = new Date(date.split(" ")[0])
      } else {
        tmpDate = date;
      }
      return tmpDate instanceof Date && !isNaN(tmpDate.valueOf());
    } catch (error) {
      return false;
    }
  }

  requestErrorHandler(err) {
    console.log("***RequestERROR***", err);

    let msg = err && err['error'] && err['error']['message'];
    if (err && err['status'] == 422) {
      // this.toastr.warning(msg || this.sDic.getText('msg-not-possible-process-data'));
      this.toastr.warning(this.sDic.getText('msg-not-possible-process-data'));
    } else if (err['status'] == 500) {
      this.toastr.error(this.sDic.getText('msg-server-error'));
    }
    else {
      // this.toastr.error(this.sDic.getText('msg-not-possible-process-data'));
      this.toastr.error(msg || this.sDic.getText('msg-not-possible-process-data'));
    }
  }
  dateToTimestamp(date, cutLast3 = false) {
    try {
      let tmpDate = new Date(date);
      if (cutLast3)
        return (tmpDate.getTime() + "").slice(0, -3);
      else
        return (tmpDate.getTime() + "");
    } catch (error) {
      return null;
    }
  }
  getDateTimestamp(date = "", withTime = false, cutLast3 = false) {
    try {
      let tmpDate = (date as string).split(' ')[0];
      let tmpTime = (date as string).split(' ')[1];

      let day = parseInt(tmpDate.split('-')[2]);
      let mounth = (parseInt(tmpDate.split('-')[1]) - 1) < 0 ? 0 : (parseInt(tmpDate.split('-')[1]) - 1);
      let year = parseInt(tmpDate.split('-')[0]);

      let d = null;
      if (withTime) {
        let hour = parseInt(tmpTime.split(':')[0]);
        let minute = parseInt(tmpTime.split(':')[1]);
        let second = parseInt(tmpTime.split(':')[2]);
        d = new Date(year, mounth, day, hour, minute, second);
      }
      else
        d = new Date(year, mounth, day);
      return d.getTime();
    } catch (error) {
      return null;
    }
  }
}
