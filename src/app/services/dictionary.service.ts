import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import dictionary from "../../config/dictionary.json";
import config from "../../config/config.json";

@Injectable({
  providedIn: 'root',

})
export class DictionaryService {
  private _availableLangs = this.getKeys();
  private _defaultLang = this._availableLangs[0];
  public _dictionary = dictionary[this._defaultLang];
  private _language = this._defaultLang;
  private _keys = this._defaultLang;
  private _fixedLang = config.fixedLang;

  private static instance: DictionaryService;

  constructor(
    // private http: HttpClient
  ) {


    let tmpLang = this.getLanguageLocalStorage();
    if (this._fixedLang && this._availableLangs.includes(this._fixedLang)) {
      tmpLang = this._fixedLang;
    } else if (this._availableLangs.includes(tmpLang)) {

    } else {
      tmpLang = this._defaultLang;
    }
    this.setDictionary(tmpLang);
    this.setLanguage(tmpLang);
    this.setLanguageLocalStorage(tmpLang);
  }
  static getInstance() {
    if (!DictionaryService.instance) {
      DictionaryService.instance = new DictionaryService();
    }
    return DictionaryService.instance;
  }
  getText(key = '', val = ""): string {
    let tmpText: string = this.getDictionary()[key];
    if (!key || !tmpText) {
      return this.getDictionary()['no-key'];
    }
    if (/\$\$/.test(tmpText)) {
      tmpText = tmpText.replace('$$', val.toString());
    }
    return tmpText;
  }
  getDictionary() {
    return this._dictionary;
  }
  getDictionaryAll() {
    return dictionary;
  }

  setDictionary(tmp) {
    this._dictionary = dictionary[tmp];
    this.setLanguageLocalStorage(tmp);
    this.setLanguage(tmp);
  }
  getLanguage() {
    return this._language;
  }
  setLanguage(language) {
    this._language = language;
  }
  getLanguageLocalStorage() {
    return localStorage.getItem('lang');
  }
  setLanguageLocalStorage(language) {
    localStorage.setItem("lang", language);
  }
  getKeys() {
    return Object.keys(dictionary);
  }
  setUpDictionary() {
    let tmpLang = this.getLanguageLocalStorage();
    if (this._availableLangs.includes(tmpLang)) {

    } else {
      tmpLang = this._defaultLang;
    }
    this.setDictionary(tmpLang);
    this.setLanguage(tmpLang);
    this.setLanguageLocalStorage(tmpLang);
  }
}
