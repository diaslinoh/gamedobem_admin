import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import {HttpCli} from ""
import config from "../../config/urls.json";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public http: HttpClient
  ) { }

  getTestJson() {
    return this.http.get(config.urlBase);
  }

  doLogin(data, spinner = false) {
    return this.http.post(config.urlBase + config.login, data, { reportProgress: spinner });
  }
  getUsers(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.users, data, { reportProgress: spinner });
  }
  getUser(id = {}, spinner = false) {
    return this.http.post(config.urlBase + config.user + id, {}, { reportProgress: spinner });
  }
  updateUser(data, spinner = false) {
    return this.http.post(config.urlBase + config.userUpdate, data, { reportProgress: spinner });
  }
  getUsersExtra(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.userExtra, data, { reportProgress: spinner });
  }
  deleteUser(id, spinner = false) {
    return this.http.post(config.urlBase + config.userDelete + id, {}, { reportProgress: spinner });
  }
  addUser(data, spinner = false) {
    return this.http.post(config.urlBase + config.userAdd, data, { reportProgress: spinner });
  }

  getEmployees(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.employees, data, { reportProgress: spinner });
  }
  getEmployeesFilter(page = 1, data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.employeesFilter + "?page=" + page, data, { reportProgress: spinner });
  }
  getEmployeesPaginate(page = 1, data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.employeesPaginate + "?page=" + page, data, { reportProgress: spinner });
  }
  getEmployee(id, spinner = false) {
    return this.http.post(config.urlBase + config.employee + id, {}, { reportProgress: spinner });
  }
  addEmployee(data, spinner = false) {
    return this.http.post(config.urlBase + config.employeeAdd, data, { reportProgress: spinner });
  }
  updateEmployee(data, spinner = false) {
    return this.http.post(config.urlBase + config.employeeUpdate, data, { reportProgress: spinner });
  }
  deleteEmployee(id, spinner = false) {
    return this.http.post(config.urlBase + config.employeeDelete + id, {}, { reportProgress: spinner });
  }
  getEmployeesExtra(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.employeesExtra, data, { reportProgress: spinner });
  }


  getCompanies(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.companies, data, { reportProgress: spinner });
  }
  getCompanie(id, spinner = false) {
    return this.http.post(config.urlBase + config.companie + id, {}, { reportProgress: spinner });
  }
  addCompanie(data, spinner = false) {
    return this.http.post(config.urlBase + config.companieAdd, data, { reportProgress: spinner });
  }
  updateCompanie(data, spinner = false) {
    return this.http.post(config.urlBase + config.companieUpdate, data, { reportProgress: spinner });
  }
  deleteCompanie(id, spinner = false) {
    return this.http.post(config.urlBase + config.companieDelete + id, {}, { reportProgress: spinner });
  }


  getProducts(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.products, data, { reportProgress: spinner });
  }
  getProduct(id, spinner = false) {
    return this.http.post(config.urlBase + config.product + id, {}, { reportProgress: spinner });
  }
  addProduct(data, spinner = false) {
    return this.http.post(config.urlBase + config.productAdd, data, { reportProgress: spinner });
  }
  updateProduct(data, spinner = false) {
    return this.http.post(config.urlBase + config.productUpdate, data, { reportProgress: spinner });
  }
  deleteProduct(id, spinner = false) {
    return this.http.post(config.urlBase + config.productDelete + id, {}, { reportProgress: spinner });
  }
  getProductsExtra(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.productsExtra, data, { reportProgress: spinner });
  }



  getFaqs(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.faqs, data, { reportProgress: spinner });
  }
  getFaq(id, spinner = false) {
    return this.http.post(config.urlBase + config.faq + id, {}, { reportProgress: spinner });
  }
  getFaqExtra(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.faqExtra, data, { reportProgress: spinner });
  }
  addFaq(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.faqAdd, data, { reportProgress: spinner });
  }
  updateFaq(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.faqUpdate, data, { reportProgress: spinner });
  }
  deleteFaq(id, spinner = false) {
    return this.http.post(config.urlBase + config.faqDelete + id, {}, { reportProgress: spinner });
  }

  getMissions(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missions, data, { reportProgress: spinner });
  }
  getMission(id = {}, spinner = false) {
    return this.http.post(config.urlBase + config.mission + id, {}, { reportProgress: spinner });
  }
  deleteMission(id = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionDelete + id, {}, { reportProgress: spinner });
  }
  addMission(data, spinner = false) {
    return this.http.post(config.urlBase + config.missionAdd, data, { reportProgress: spinner });
  }
  updateMission(data, spinner = false) {
    return this.http.post(config.urlBase + config.missionUpdate, data, { reportProgress: spinner });
  }
  getMissionExtra(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionExtra, data, { reportProgress: spinner });
  }
  getMissionsFilter(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionsFilter, data, { reportProgress: spinner });
  }


  getMissionsReal(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionsReal, data, { reportProgress: spinner });
  }
  getMissionReal(id, spinner = false) {
    return this.http.post(config.urlBase + config.missionReal + id, {}, { reportProgress: spinner });
  }
  getMissionsRealPaginate(page = 1, data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionsRealPaginate + "?page=" + page, data, { reportProgress: spinner });
  }
  statusMissionReal(data, spinner = false) {
    return this.http.post(config.urlBase + config.missionRealStatus, data, { reportProgress: spinner });
  }
  getMissionsRealFilter(data = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionsRealFilter, data, { reportProgress: spinner });
  }
  deleteMissionReal(id = {}, spinner = false) {
    return this.http.post(config.urlBase + config.missionRealDelete + id, {}, { reportProgress: spinner });
  }
}
