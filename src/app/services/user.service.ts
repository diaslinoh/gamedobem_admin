import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _user: any = null;
  private _tokenName: string = "token";
  private _userName: string = "user";

  constructor(
    private cookieService: CookieService
  ) {
    // console.log(this.getUserLocal().length > 0);

    if (this.getUserLocal() && Object.keys(this.getUserLocal()).length > 0 && this.getToken()) {
      console.log("User and Token");
      this.setUser(this.getUserLocal());
    } else {
      console.log("NO User and Token");
    }
  }

  getUser(): any {
    return this._user;
  }
  setUser(user: any = null) {
    this._user = user;
    this.setUserLocal(user);
  }
  getUserLocal(): any {
    let tmpUser = localStorage.getItem(this._userName);
    if (tmpUser && typeof tmpUser == 'string' && tmpUser.length > 0) {
      try {
        return JSON.parse(localStorage.getItem(this._userName));
      } catch (err) {
        return false;
      }
    }

    return false;
  }
  setUserLocal(user: any = null) {
    if (user)
      localStorage.setItem(this._userName, JSON.stringify(user));
    else
      localStorage.removeItem(this._userName);
    return this;
  }

  setToken(token = null) {
    if (token)
      this.cookieService.set(this._tokenName, token, 999);
    else
      this.cookieService.delete(this._tokenName);
    return this;
  }
  getToken() {
    return this.cookieService.get(this._tokenName);
  }

  checkUserStatus() {
    // console.log(this.getUser());

    if (this.getUser() instanceof Object && Object.keys(this.getUser()).length > 0 && this.getUserLocal() && Object.keys(this.getUserLocal()).length > 0 && typeof this.getToken() == 'string' && this.getToken().length > 0)
      return true;
    return false;
  }

  setupUser(user, token) {
    console.log(user, token);

    this.setUser(user);
    this.setToken(token);
    return this;
  }

  destroyUser() {
    this.setUser(null);
    this.setToken(null);
    return this;
  }
}
