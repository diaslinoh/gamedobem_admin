import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DictionaryService } from '../../services/dictionary.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, NgForm } from '@angular/forms';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  
  
  constructor(
    private aRoute: ActivatedRoute,
    private utils: UtilsService,
    public sDic: DictionaryService,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    // console.log(this.aRoute.snapshot.data);
  }
  goLogin() {
    this.utils.goUrl();
  }
}
