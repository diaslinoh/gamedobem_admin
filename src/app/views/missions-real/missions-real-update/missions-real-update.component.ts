import { Component, OnInit, ViewChild } from '@angular/core';
import { DictionaryService } from '../../../services/dictionary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { UtilsService } from '../../../services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm, NgModel, ControlContainer, FormControl } from '@angular/forms';
import { Subject, throwError, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { filter } from 'minimatch';

import urls from "../../../../config/urls.json";

@Component({
  selector: 'app-missions-real-update',
  templateUrl: './missions-real-update.component.html',
  styleUrls: ['./missions-real-update.component.scss']
})
export class MissionsRealUpdateComponent implements OnInit {



  @ViewChild('f', { static: false }) f: NgForm;
  public idData = null;
  public data = null;
  public dataExtra = null;

  public bsConfig = null;

  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public sApi: ApiService,
    public sUtils: UtilsService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
  ) {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
  }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idData = item.id;
    });
  }
  ngAfterViewInit() {
    if ((this.idData + ""))
      this.startData();
    else {
      this.dataExtra = undefined;
    }
    // console.log(this.f);
    setTimeout(() => {
      if (this.f) {
        this.initializeForm(this.f);
      }
    }, 0);

  }
  initializeForm(form: NgForm) {
    // form.controls['quantidade'].setValue(0);
  }
  fillForm(data) {


  }
  startData() {
    this.sApi.getMissionReal(this.idData)
      .subscribe(res => {
        console.log(res);
        this.data = res['data'];
        this.fillForm(this.data);
      }, err => {
        console.log(err);
        this.toastr.error(this.sDic.getText("common-data-not-loaded"))
        this.dataExtra = undefined;
      });
    // this.sApi.getFaqExtra()
    //   .subscribe(res => {
    //     console.log(res);
    //     this.dataExtra = res['data'];
    //     if (this.idData) {
    //       this.sApi.getFaq(this.idData)
    //         .pipe(tap((x) => {
    //           console.log(x);
    //           if (x && x['data'] && x['data']['dados']) {

    //           } else {
    //             // throwError("Oops!");
    //             throw Observable.throw('Oops!');
    //           }
    //         }))
    //         .subscribe(res2 => {
    //           console.log(res2);
    //           // this.dataExtra = res['data'];
    //           this.data = res2['data']['dados'];
    //           this.fillForm(this.data);
    //         }, err => {
    //           console.log(err);
    //           this.toastr.error(this.sDic.getText("common-data-not-loaded"))
    //           this.dataExtra = undefined;
    //         })
    //     } else {

    //     }
    //   }, err => {
    //     console.log(err);
    //     this.toastr.error(this.sDic.getText("common-data-not-loaded"))
    //     this.dataExtra = undefined;

    //   });



  }
  onSubmit(form: NgForm, e: Event, status) {
    // e.preventDefault();
    console.log(form);
    // form.control.s

    if (form.valid) {
      let formValue = Object.assign({}, form.value);

      if (this.idData) {
        formValue.id_rel = this.idData;
        console.log(formValue);
        let dataToSend = {
          id_rel: this.idData,
          status: status,
          motivo: form.controls['motivo'] && form.controls['motivo'].value,
          id_mensagem: form.controls['just'] && form.controls['just'].value,
        };

        this.sApi.statusMissionReal(dataToSend)
          .subscribe(res => {
            this.data.missao_rel.status = status;
            this.toastr.success(this.sDic.getText("msg-success-update-register"));
          }, err => {
            console.log(err);
            this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
          });

      } else {
        // this.sApi.addFaq(formValue)
        //   .subscribe(res => {
        //     this.toastr.success(this.sDic.getText("msg-success-save-register"));
        //   }, err => {
        //     console.log(err);
        //     this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
        //   });
      }
    } else {
      this.toastr.warning(this.sDic.getText('msg-form-invalid'));
      let tmpInvalidField = document.querySelector(".ng-invalid:not(form):not(div)");
      if (tmpInvalidField) {
        tmpInvalidField.scrollIntoView();
        tmpInvalidField['focus']();
      }
    }

  }
  cutSize(size, el: NgModel) {
    // console.log(size, el);
    let tmpSub = new Subject();
    tmpSub
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(res => {
        el.control.setValue((el.value + "").replace(/\D/ig, '').substr(0, size));
      })
    tmpSub.next();

  }

}
