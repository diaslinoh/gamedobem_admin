import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsRealUpdateComponent } from './missions-real-update.component';

describe('MissionsRealUpdateComponent', () => {
  let component: MissionsRealUpdateComponent;
  let fixture: ComponentFixture<MissionsRealUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsRealUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsRealUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
