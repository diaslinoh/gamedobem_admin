import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsRealComponent } from './missions-real.component';

describe('MissionsRealComponent', () => {
  let component: MissionsRealComponent;
  let fixture: ComponentFixture<MissionsRealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsRealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsRealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
