import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DictionaryService } from '../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from '../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RemoveConfirmComponent } from '../../modals/remove-confirm/remove-confirm.component';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  public data: Array<any> = null;
  public dataFilter: Array<any> = null;
  public dataExtraany = null;

  public itemsPerPage = 10;
  public currentPage = 1;
  public searchChanged: Subject<string> = new Subject<string>();
  public modelFilters = { search: "", published: null };


  public bsConfig = null;
  public modalRemoveUserRef: BsModalRef;



  constructor(
    private sApi: ApiService,
    public sDic: DictionaryService,
    public spinner: NgxSpinnerService,
    public sUtils: UtilsService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public modalService: BsModalService,
    public toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
    this.startData();
  }
  startData() {
    this.sApi.getCompanies()
      .subscribe(res => {
        console.log(res);
        this.data = res['data'];
        this.dataFilter = this.data.slice();
      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.data = undefined;
      });
  }

  doSearch(txt) {
    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(res => {
        this.doAllFilters();
      });
    this.searchChanged.next(txt);
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.dataFilter = this.dataFilter.filter(res => {
      let tmpNome = (res['nome'] ? res['nome'].toLowerCase() : "");
      tmpNome = tmpNome.replace(/  /gi, " ");
      return tmpNome.indexOf(ev) > -1;
    });
  }
  filterPublished(published) {
    console.log(published);

    if (published == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      if (el.publicado == published)
        return true;
    });
  }

  doAllFilters() {
    this.currentPage = 1;
    setTimeout(() => {
      this.dataFilter = this.data.slice();
      this.filterSearch(this.modelFilters.search);
      this.filterPublished(this.modelFilters.published);
    }, 100);
  }
  clearFilters() {
    this.modelFilters.search = "";
    this.modelFilters.published = null;
    this.doAllFilters();
  }
  editData(data) {
    this.router.navigate(['update/' + data.id_emp], { relativeTo: this.aRoute });
  }

  deleteData(data) {
    this.modalRemoveUserRef = this.modalService.show(RemoveConfirmComponent, { class: 'modal-dialog-centered' });
    this.modalRemoveUserRef.content.bsModalRef.content.onConfirm.subscribe(res => {
      // console.log("confirmed: ", data);
      // this.sApi.deleteUser(data.id)
      this.spinner.show('spinner-extra');
      this.sApi.deleteCompanie(data.id_emp)
        .pipe(finalize(() => {
          this.spinner.hide('spinner-extra');
        }))
        .subscribe(res => {
          let tmpData = this.data.indexOf(data);
          let tmpDataFilter = this.dataFilter.indexOf(data);
          if (tmpData > -1)
            this.data.splice(tmpData, 1);
          if (tmpDataFilter > -1)
            this.dataFilter.splice(tmpDataFilter, 1);
          data = null;
          this.modalRemoveUserRef.hide();
          this.toastr.success(this.sDic.getText('msg-success-exclude'));

        }, err => {
          this.toastr.warning(this.sDic.getText('msg-not-possible-exclude-register'));
          // this.sUtils.requestErrorHandler(err);
        });
    });
  }

  pageChanged(ev) {
  }
}
