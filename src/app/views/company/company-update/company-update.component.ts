import { Component, OnInit, ViewChild } from '@angular/core';
import { DictionaryService } from '../../../services/dictionary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { UtilsService } from '../../../services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm, NgModel, ControlContainer, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { filter } from 'minimatch';

import urls from "../../../../config/urls.json";

@Component({
  selector: 'app-company-update',
  templateUrl: './company-update.component.html',
  styleUrls: ['./company-update.component.scss']
})
export class CompanyUpdateComponent implements OnInit {


  @ViewChild('f', { static: false }) f: NgForm;
  public idData = null;
  public data = null;
  public dataExtra = null;



  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public sApi: ApiService,
    public sUtils: UtilsService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idData = item.id;
    });
  }
  ngAfterViewInit() {
    this.startData();
    // console.log(this.f);
    setTimeout(() => {
      if (this.f) {
        this.initializeForm(this.f);
      }
    }, 0);

  }
  initializeForm(form: NgForm) {
    // form.controls['quantidade'].setValue(0);
  }
  fillForm(data) {
    this.f.controls['dominios'].setValue(data['dominios']);
    this.f.controls['nome'].setValue(data['nome']);
    this.f.controls['publicado'].setValue(data['status']);
  }
  startData() {
    if (this.idData) {
      this.sApi.getCompanie(this.idData)
        .subscribe(res2 => {
          console.log(res2);
          this.data = res2[0];
          // this.spinner.hide('spinner-extra');
          this.fillForm(this.data);
        }, err => {
          // this.spinner.hide('spinner-extra');
          this.toastr.error(this.sDic.getText("common-data-not-loaded"))
          this.dataExtra = undefined;
          // this.router.navigate(['/products']);
        });
    }
    // this.spinner.show('spinner-extra');
    // this.sApi.getProductsExtra()
    //   .subscribe(res => {
    //     console.log(res);
    //     this.dataExtra = res;
    //     if (this.idData) {
    //       this.sApi.getProduct(this.idData)
    //         .subscribe(res2 => {
    //           console.log(res2);
    //           this.data = res2['data']['dados'][0];
    //           this.spinner.hide('spinner-extra');
    //           this.fillForm(this.data);
    //         }, err => {
    //           this.spinner.hide('spinner-extra');
    //           this.toastr.error(this.sDic.getText("common-data-not-loaded"))
    //           this.dataExtra = undefined;
    //           // this.router.navigate(['/products']);
    //         });
    //     } else {
    //       this.spinner.hide('spinner-extra');
    //     }
    //   }, err => {
    //     this.spinner.hide('spinner-extra');
    //     this.toastr.error(this.sDic.getText("common-data-not-loaded"));
    //     this.dataExtra = undefined;
    //     // this.router.navigate(['/products']);
    //   });
  }
  onSubmit(form: NgForm, e: Event) {
    e.preventDefault();
    console.log(form);
    if (form.valid) {
      let formValue = Object.assign({}, form.value);


      if (this.idData) {
        formValue.id_emp = this.idData;
        this.sApi.updateCompanie(formValue)
          .subscribe(res => {
            this.toastr.success(this.sDic.getText("msg-success-update-register"));
          }, err => {
            console.log(err);

            this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
          });
      } else {
        this.sApi.addCompanie(formValue)
          .subscribe(res => {
            this.toastr.success(this.sDic.getText("msg-success-save-register"));
          }, err => {
            console.log(err);
            this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
          });
      }
    } else {
      this.toastr.warning(this.sDic.getText('msg-form-invalid'));
      let tmpInvalidField = document.querySelector(".ng-invalid:not(form)");
      if (tmpInvalidField) {
        tmpInvalidField.scrollIntoView();
        tmpInvalidField['focus']();
      }
    }

  }
  cutSize(size, el: NgModel) {
    // console.log(size, el);
    let tmpSub = new Subject();
    tmpSub
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(res => {
        el.control.setValue((el.value + "").replace(/\D/ig, '').substr(0, size));
      })
    tmpSub.next();

  }
}
