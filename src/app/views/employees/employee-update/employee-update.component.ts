import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DictionaryService } from '../../../services/dictionary.service';
import { NgForm, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UtilsService } from '../../../services/utils.service';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.scss']
})
export class EmployeeUpdateComponent implements OnInit {
  @ViewChild('f', { static: false }) public f: NgForm;
  public idFunc = null;
  public employeesExtra = null;

  public employeeData = null;

  public bsConfig = null;
  public cities: Array<any> = null;
  public modelForm = {
    nome: null,
    sobrenome: null,
    email: null,
    empresa: null,
    voluntario: null,
    senha: null,
    genero: null,
    dt_nasc: null,
    id_estado: null,
    estado: null,
    id_cidade: null,
    cidade: null,
  };



  constructor(
    private sApi: ApiService,
    private sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public toastr: ToastrService,
    public sUtils: UtilsService,
    public spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };

    this.aRoute.params.forEach((item) => {
      this.idFunc = item.id;
    });

    this.spinner.show('spinner-extra');
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.f.resetForm();
      this.f.controls['deficiencia'].setValue(0);
      this.f.controls['pne'].setValue(0);
      // this.f.controls['json_info'].setValue(0);
      this.startEmployeeExtra()
        .subscribe(res => {
          if (this.idFunc) {
            this.getEmployee();
            this.f.controls['senha'].setValidators(null);
            this.f.controls['senha'].setErrors(null);
          } else {
            this.spinner.hide('spinner-extra');
          }
        }, err => {
          this.toastr.error(this.sDic.getText('common-data-not-loaded'));
          this.spinner.hide('spinner-extra');
          // this.employeesExtra = undefined;
        });

    }, 0);
  }
  startForm() {
    Object.keys(this.f.controls).forEach(el => {
      this.f.controls[el].setValue(null);
    });
  }
  fillForm(employee) {
    // console.log(employee);
    this.f.controls['nome'].setValue(employee['nome']);
    this.f.controls['sobrenome'].setValue(employee['sobrenome']);
    this.f.controls['email'].setValue(employee['email']);
    this.f.controls['voluntario'].setValue(employee['voluntario']);
    this.f.controls['genero'].setValue((employee['genero'] && (employee['genero'] + "").toLowerCase()));
    this.f.controls['id_emp'].setValue(employee['id_emp']);
    let tmpDataNasc = new Date(this.sUtils.getDateTimestamp(employee['dt_nasc']));
    this.f.controls['dt_nasc'].setValue(tmpDataNasc);
    this.f.controls['deficiencia'].setValue(employee['deficiencia'] ? employee['deficiencia'] : 0);
    this.f.controls['id_deficiencia'].setValue(employee['id_deficiencia'] == 0 ? null : employee['id_deficiencia']);
    this.f.controls['pne'].setValue(employee['pne']);
    this.f.controls['id_pne'].setValue(employee['id_pne'] == 0 ? null : employee['id_pne']);
    this.f.controls['tipo'].setValue(employee['tipo'] == 0 ? null : employee['tipo']);


    this.f.controls['id_estado'].setValue(employee['id_estado']);
    this.stateChange(employee['id_estado']);
    if (employee['id_estado']) {
      this.f.controls['id_cidade'].setValue(employee['id_cidade']);
      this.cityChange(employee['id_cidade']);
    }
  }
  getEmployee() {
    this.sApi.getEmployee(this.idFunc, true)
      .subscribe(res => {
        this.employeeData = res['data']['dados'];
        this.fillForm(this.employeeData);
        this.spinner.hide('spinner-extra');
      }, err => {
        console.log(err);
        this.toastr.error(this.sDic.getText('common-data-not-loaded'));
        this.spinner.hide('spinner-extra');
        this.employeesExtra = undefined;
      })
  }
  startEmployeeExtra() {

    return new Observable((obs) => {
      this.sApi.getEmployeesExtra({}, true)
        .subscribe(res => {
          // console.log(res);
          this.employeesExtra = res;
          // this.toastr.success("Success");
          obs.next(true);
        }, err => {
          console.log(err);
          this.toastr.error(this.sDic.getText('common-data-not-loaded'));
          obs.error(false);
          this.spinner.hide('spinner-extra');
          this.employeesExtra = undefined;
        });

    });

  }
  stateChange(id_estado) {
    // console.log(id_estado);
    this.f.controls['estado'].setValue(null);
    this.f.controls['id_cidade'].setValue(null);
    this.f.controls['cidade'].setValue(null);

    this.employeesExtra['estados'].find(el => {
      if (el.id == id_estado) {
        this.f.controls['estado'].setValue(el.uf);
        return true;
      }

    });

    this.cities = this.employeesExtra['cidades'].filter(el => {
      // return el.id_estado == id_estado.value;
      return el.estado == id_estado;
    });
  }
  cityChange(id_cidade) {
    // console.log(id_cidade);

    let tmpCity = this.cities.find(el => {
      if (el.id == id_cidade) {
        this.f.controls['cidade'].setValue(el.nome);
        return true;
      }
    });
    if (!tmpCity) {
      this.f.controls['id_cidade'].setValue(null);
      this.f.controls['cidade'].setValue(null);
    }
  }

  pcdChange(id_deficiencia) {
    // console.log(id_deficiencia);
    if (id_deficiencia.value == null) {
      this.f.controls['deficiencia'].setValue(0);
    } else {
      this.f.controls['deficiencia'].setValue(1);
    }
  }
  pneChange(id_pne) {
    // console.log(id_pne);
    if (id_pne.value == null) {
      this.f.controls['pne'].setValue(0);
    } else {
      this.f.controls['pne'].setValue(1);
    }
  }
  sendForm(form: NgForm) {

    if (form.valid) {
      let tmpData = Object.assign({}, form.value);
      tmpData.tipo = tmpData.tipo ? tmpData.tipo : 0;
      tmpData.id_deficiencia = tmpData.id_deficiencia ? tmpData.id_deficiencia : 0;
      tmpData.id_pne = tmpData.id_pne ? tmpData.id_pne : 0;
      tmpData.email = (tmpData.email + "").toLowerCase();
      let tmpDtNasc = (new Date((tmpData.dt_nasc + ""))).getTime() + "";
      tmpDtNasc = tmpDtNasc.substr(0, tmpDtNasc.length - 3);
      tmpData.dt_nasc = tmpDtNasc;

      if (tmpData.id_estado == null)
        delete tmpData.id_estado;
      if (tmpData.estado == null)
        delete tmpData.estado;
      if (tmpData.cidade == null)
        delete tmpData.cidade;
      if (tmpData.id_cidade == null)
        delete tmpData.id_cidade;
      if (tmpData.genero == null)
        delete tmpData.genero;
      if (tmpData.id_emp == null)
        delete tmpData.id_emp;
      if (tmpData.voluntario == null)
        delete tmpData.voluntario;
      // if (tmpData.tipo == null)
      //   delete tmpData.tipo;
      if (tmpData.json_info == null)
        delete tmpData.json_info;
      if (!tmpData.senha)
        delete tmpData.senha;
      // tmpData.id_emp = tmpData.id_emp ? tmpData.id_emp : 0;
      // tmpData.estado = (tmpData.estado + "").substr(0, 2);





      if (this.idFunc) {
        tmpData.id_func = this.idFunc;
        console.log(tmpData);
        this.sApi.updateEmployee(tmpData)
          .subscribe(res => {
            console.log(res);
            // this.toastr.success(res['message']);
            this.toastr.success(this.sDic.getText('msg-success-update-register'));

          }, err => {
            // this.sUtils.requestErrorHandler(err);
            console.log(err);
            this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
          });
      } else {
        console.log(tmpData);
        this.sApi.addEmployee(tmpData)
          .subscribe(res => {
            console.log(res);
            // this.toastr.success(res['message']);
            this.toastr.success(this.sDic.getText('msg-success-save-register'));
          }, err => {
            // this.sUtils.requestErrorHandler(err);
            console.log(err);
            if (err && err['status'] == 422)
              this.toastr.warning(err['error']['message'] || this.sDic.getText('msg-not-possible-save-register'));
            else
              this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
          });
      }
    } else {
      let invalidField = document.querySelector(".ng-invalid:not(form)");
      if (invalidField) {
        invalidField.scrollIntoView(true);
        invalidField['focus']();
      }
      this.toastr.warning(this.sDic.getText('msg-form-invalid'))
    }

  }
}
