import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { EmployeesService } from './employees.service';
import { DictionaryService } from '../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from '../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
  providers: [
    DatePipe
  ]
})
export class EmployeesComponent implements OnInit {
  public users: Array<any> = null;
  public usersFilter: Array<any> = null;
  public usersExtra: any = null;

  public itemsPerPage = 10;
  public totalItems = 0;
  public currentPage = 1;
  public searchChanged: Subject<string> = new Subject<string>();
  public modelFilters = { nome: null, email: null, status: null, cadastro_em: null, cadastro_ate: null };
  public currFilter = {};


  public cities: Array<any> = [];
  public bsConfig = null;
  public modalRemoveUserRef: BsModalRef;
  public tmpUserDelete = null;

  constructor(
    private sApi: ApiService,
    public sEmployees: EmployeesService,
    public sDic: DictionaryService,
    public spinner: NgxSpinnerService,
    public sUtils: UtilsService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public modalService: BsModalService,
    public toastr: ToastrService,
    public pDate: DatePipe,

  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };

    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(txt => {
        this.doAllFilters();
      });

    this.startEmployees();
  }

  debounce(callback, time = 0) {
    let tmpSubject = new Subject();
    tmpSubject.pipe(
      debounceTime(time),
      distinctUntilChanged()
    ).subscribe(res => {
      callback();
      tmpSubject.unsubscribe();
    });
    tmpSubject.next("");

  }
  startEmployees() {
    this.spinner.show('spinner-extra');
    this.sApi.getEmployeesPaginate(1)
      .subscribe(res => {
        console.log(res);
        this.itemsPerPage = res['data']['to'];
        this.totalItems = res['data']['total'];
        this.users = res['data']['data'];

        this.spinner.hide('spinner-extra');
      }, err => {
        console.log(err);
        this.users = undefined;
        this.spinner.hide('spinner-extra');
      });
  }

  doSearch(txt) {
    this.searchChanged.next(txt)
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.usersFilter = (this.usersFilter as Array<any>).filter(res => {
      let tmpNome = (res['nome'] ? res['nome'].toLowerCase() : "") + " " + (res['sobrenome'] ? res['sobrenome'].toLowerCase() : "");
      tmpNome = tmpNome.replace(/  /gi, " ");
      let tmpEmpresa = (res['empresa'] ? res['empresa'].toLowerCase() : "");
      tmpEmpresa = tmpEmpresa.replace(/  /gi, " ");

      return tmpNome.indexOf(ev) > -1 || tmpEmpresa.indexOf(ev) > -1;
    });
  }


  filterStatus(f) {
    this.usersFilter = this.usersFilter.filter(el => {
      if (f == null) return true;
      return el.status == f;
    });
  }

  // filterState(stateId) {
  //   this.modelFilters.city = null;
  //   this.cities = this.getCities(stateId);
  // }

  getCities(stateId) {
    if (stateId == null) return [];
    return this.usersExtra.cidades.filter(el => {
      return el.id_estado === stateId;
      // return el.id === stateId;
    });
  }
  filterRegistrationSince(date: Date) {
    // console.log("filterRegistrationSince");

    try {
      let tmpDate = new Date(date.getFullYear(), ((date.getMonth()) < 0 ? 0 : date.getMonth()), date.getDate());
      // console.log(tmpDate);
      this.usersFilter = this.usersFilter.filter(el => {
        let currDate = this.sUtils.getDateTimestamp(el.created_at);
        // console.log(currDate);

        return currDate >= tmpDate.getTime();
      });

      // console.log(this.usersFilter);
    } catch (error) {

      return null;
    }
  }
  filterRegistrationUntil(date: Date) {
    // console.log("filterRegistrationSince");

    try {
      let tmpDate = new Date(date.getFullYear(), ((date.getMonth()) < 0 ? 0 : date.getMonth()), date.getDate());
      // console.log(tmpDate);
      this.usersFilter = this.usersFilter.filter(el => {
        let currDate = this.sUtils.getDateTimestamp(el.created_at);
        // console.log(currDate);
        return tmpDate.getTime() >= currDate;
      });
      // console.log(this.usersFilter);
    } catch (error) {
      return null;
    }
  }
  doAllFilters() {
    // this.currentPage = 1;
    // setTimeout(() => {
    //   this.usersFilter = this.users.filter(el => true);
    //   this.filterSearch(this.modelFilters.search);
    //   this.filterStatus(this.modelFilters.status);
    //   this.filterRegistrationSince(this.modelFilters.created_since);
    //   this.filterRegistrationUntil(this.modelFilters.created_until);
    // }, 100);
  }
  doFilterApi(data) {
    let tmpData = this.sUtils.mapObject(data);

    try {
      if (tmpData['cadastro_de'])
        tmpData['cadastro_de'] = this.pDate.transform(this.sUtils.dateToTimestamp(tmpData['cadastro_de']), "yyyy-MM-dd");
      if (tmpData['cadastro_ate'])
        tmpData['cadastro_ate'] = this.pDate.transform(this.sUtils.dateToTimestamp(tmpData['cadastro_ate']), "yyyy-MM-dd");
    } catch (error) {

    }
    this.currFilter = Object.assign({}, tmpData);
    // console.log(this.modelFilters);
    this.sApi.getEmployeesPaginate(1, tmpData)
      .subscribe(res => {
        this.currentPage = res['data']['current_page'];
        this.totalItems = res['data']['total'];
        this.users = res['data']['data'];
      }, err => {
        this.toastr.warning(this.sDic.getText("msg-not-possible-do-filter"));
      });
  }
  clearFilters() {
    Object.keys(this.modelFilters).forEach(el => {
      this.modelFilters[el] = null;
    });
    this.currFilter = {};
    // this.modelFilters.status = null;
    // this.modelFilters.created_since = null;
    // this.modelFilters.created_until = null;
    this.getEmployees();
    // this.doAllFilters();
  }
  editUser(user) {
    // console.log(user);
    this.router.navigate(['update/' + user.id_func], { relativeTo: this.aRoute });
  }

  deleteUser(user, template) {
    // console.log(user, template);
    this.tmpUserDelete = user;
    this.modalRemoveUserRef = this.modalService.show(template, { class: 'modal-dialog-centered' });
  }
  confirmRemoveUser() {


    this.sApi.deleteEmployee(this.tmpUserDelete.id_func)
      .subscribe(res => {
        console.log(res);
        let tmpUser = this.users.indexOf(this.tmpUserDelete);
        // let tmpUserFilter = this.usersFilter.indexOf(this.tmpUserDelete);

        if (tmpUser > -1)
          this.users.splice(this.users.indexOf(this.tmpUserDelete), 1);
        // if (tmpUserFilter > -1)
        //   this.usersFilter.splice(this.usersFilter.indexOf(this.tmpUserDelete), 1);
        this.totalItems = this.totalItems - 1;
        this.modalRemoveUserRef.hide();
        this.tmpUserDelete = null;
        this.toastr.success(this.sDic.getText('msg-success-exclude'));
      }, err => {
        this.sUtils.requestErrorHandler(err);
      });
  }
  getEmployees(page = 1) {
    this.sApi.getEmployeesPaginate(page, this.currFilter)
      .subscribe(res => {
        this.currentPage = res['data']['current_page'];
        this.totalItems = res['data']['total'];
        this.users = res['data']['data'];
      }, err => {
        console.log(err);
        this.toastr.warning(this.sDic.getText('common-data-not-loaded'))
      });
  }
  pageChanged(ev) {
    console.log(ev);
    
    this.sApi.getEmployeesPaginate(ev.page, this.currFilter)
      .subscribe(res => {
        // this.itemsPerPage = res['data']['to'];
        this.totalItems = res['data']['total'];
        this.users = res['data']['data'];
      }, err => {
        console.log(err);
        this.toastr.warning(this.sDic.getText('common-data-not-loaded'))
      });
  }
  dateSinceChange(ev) {
    this.doAllFilters();
  }
  dateUntilChange(ev) {
    this.doAllFilters();
  }

}
