import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  public users = null;
  public user = null;
  constructor() { }

  getUsers() {
    return this.users;
  }
  setUsers(users) {
    this.users = users;
  }
  getUser() {
    return this.user;
  }
  setUser(user) {
    this.user = user;
  }
}
