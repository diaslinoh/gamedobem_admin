import { TestBed } from '@angular/core/testing';

import { UsersCommonService } from './users-common.service';

describe('UsersCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersCommonService = TestBed.get(UsersCommonService);
    expect(service).toBeTruthy();
  });
});
