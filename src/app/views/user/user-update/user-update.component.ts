import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DictionaryService } from '../../../services/dictionary.service';
import { NgForm, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UtilsService } from '../../../services/utils.service';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  @ViewChild('f', { static: false }) public f: NgForm;
  public idUser = null;
  public usersExtra = null;




  constructor(
    private sApi: ApiService,
    private sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public toastr: ToastrService,
    public sUtils: UtilsService,
    public spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idUser = item.id;
    });

  }
  getUser() {
    this.sApi.getUser(this.idUser)
      .subscribe(res => {
        console.log(res);
        let tmpUser = res['data']['admin'];
        let tmpPermissions = res['data']['permissoes'];
        tmpPermissions = tmpPermissions instanceof Array ? tmpPermissions : [tmpPermissions];

        this.f.controls['nome'].setValue(tmpUser['nome']);
        this.f.controls['login'].setValue(tmpUser['login']);
        this.f.controls['id_idioma'].setValue(tmpUser['id_idioma']);
        this.f.controls['status'].setValue(tmpUser['status']);
        this.f.controls['super'].setValue(tmpUser['super']);
        this.f.controls['senha'].setValidators(null);
        this.f.controls['senha'].setErrors(null);
        let tmpSecaoModel = { include: 0, edit: 0, exclude: 0 };
        let tmpSecao = this.f.controls['secao']['controls'];
        // Object.keys(tmpSecao).forEach(el => {
        //   tmpSecao[el].setValue(tmpSecaoModel);
        // });

        tmpPermissions.forEach(el => {

          if (tmpSecao[el.id_secao]) {
            tmpSecao[el.id_secao].setValue(
              { include: el.perm_incluir, edit: el.perm_editar, exclude: el.perm_excluir }
            );
          }
        })

        // this.f.controls['secao']['controls']['3'].setValue({ include: 0, edit: 0, exclude: 0 });
      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.usersExtra = undefined;
      });
  }
  getUsersExtra() {

    this.sApi.getUsersExtra()
      .subscribe(res => {
        // console.log(res);
        this.usersExtra = res['message'];
        if (this.idUser)
          this.getUser();
      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.usersExtra = undefined;
        // console.log(err);
      });

  }
  ngAfterViewInit() {
    setTimeout(() => {
      console.log(this.f);

      this.f.resetForm();
      this.f.controls['super'].setValue(0);
      this.f.controls['status'].setValue(0);
      this.getUsersExtra();
    }, 0);
  }
  sendForm(form: NgForm) {
    console.log(form);
    if (form.valid) {

      let tmpForm = Object.assign({}, form.value);
      if (!tmpForm.senha)
        delete tmpForm.senha;
      let secao = tmpForm.secao;
      tmpForm.id_secao = [];
      tmpForm.perm_incluir = [];
      tmpForm.perm_editar = [];
      tmpForm.perm_excluir = [];

      tmpForm.id_secao = Object.keys(secao).map(el => {
        tmpForm.perm_incluir.push(secao[el].include ? 1 : 0);
        tmpForm.perm_editar.push(secao[el].edit ? 1 : 0);
        tmpForm.perm_excluir.push(secao[el].exclude ? 1 : 0);
        return el;
      });
      delete tmpForm.secao;

      if (this.idUser) {
        tmpForm.id = this.idUser;
        console.log(tmpForm);
        this.sApi.updateUser(tmpForm)
          .subscribe(res => {
            console.log(res);
            this.toastr.success(res['message'] || this.sDic.getText('msg-success-update'));
          }, err => {
            this.sUtils.requestErrorHandler(err);
          });
      } else {
        console.log(tmpForm);
        this.sApi.addUser(tmpForm)
          .subscribe(res => {
            console.log(res);
            this.toastr.success(res['message'] || this.sDic.getText('msg-success-register'));
          }, err => {
            this.sUtils.requestErrorHandler(err);
          });
      }
    } else {
      let invalidField = document.querySelector(".ng-invalid:not(form)");
      if (invalidField) {
        invalidField.scrollIntoView(true);
        invalidField['focus']();
      }
      this.toastr.warning(this.sDic.getText('msg-form-invalid'))
    }

  }
}
