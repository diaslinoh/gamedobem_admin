import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { UsersCommonService } from './users-common.service';
import { DictionaryService } from '../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
// import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { RemoveConfirmComponent } from '../../modals/remove-confirm/remove-confirm.component';
import { UtilsService } from '../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']

})
export class UserComponent implements OnInit {
  public users: Array<any> = null;
  public usersFilter: Array<any> = null;
  public itemsToDisplay = 3;
  public itemsPerPage = 10;
  public currentPage = 1;
  public search = "";
  public searchChanged: Subject<string> = new Subject<string>();
  public fPublished = null;

  public modelFilters = { search: "", published: null };

  public modalRemoveUserRef: BsModalRef;

  public tmpUserDelete = null;
  constructor(
    private sApi: ApiService,
    private sUtils: UtilsService,
    public sUsersCommon: UsersCommonService,
    public sDic: DictionaryService,
    public modalService: BsModalService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(txt => {
        this.doAllFilters();
      });

    this.startUsers();
  }
  startUsers() {
    console.log('startUsers');

    this.sApi.getUsers({})
      .subscribe(res => {
        console.log(res['data']);
        this.sUsersCommon.setUsers(res['data']);
        this.users = this.sUsersCommon.getUsers();
        this.usersFilter = this.users.filter(el => { return true });
      }, err => {
        console.log(err);
        this.users = undefined;
      });
  }
  doSearch(txt) {
    this.searchChanged.next(txt)
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.usersFilter = (this.usersFilter as Array<any>).filter(res => {
      let tmpNome = (res['nome'] ? res['nome'].toLowerCase() : "");
      tmpNome = tmpNome.replace(/  /gi, " ");
      let tmpLogin = res['login'] ? res['login'].toLowerCase() : "";
      tmpLogin = tmpLogin.replace(/  /gi, " ");
      return tmpNome.indexOf(ev) > -1 || tmpLogin.indexOf(ev) > -1;
    });
  }


  filterPublished(f) {
    this.usersFilter = this.usersFilter.filter(el => {
      if (f == null) return true;
      return el.status == f;
    });
  }
  doAllFilters() {
    this.currentPage = 1;
    this.usersFilter = this.users.filter(el => true);
    this.filterSearch(this.modelFilters.search);
    this.filterPublished(this.modelFilters.published);
  }
  clearFilters() {
    this.modelFilters.search = "";
    this.modelFilters.published = null;
    this.doAllFilters();
  }
  editUser(user) {
    // console.log(user);
    this.router.navigate(['update/' + user.id], { relativeTo: this.aRoute })
  }
  deleteUser(user, template) {
    this.modalRemoveUserRef = this.modalService.show(RemoveConfirmComponent, { class: 'modal-dialog-centered' });
    this.modalRemoveUserRef.content.bsModalRef.content.onConfirm.subscribe(res => {
      console.log(user);


      this.sApi.deleteUser(user.id)
        .subscribe(res => {

          let tmpUserIndex = this.users.indexOf(user);
          let tmpUserFiltersIndex = this.usersFilter.indexOf(user);
          if (tmpUserIndex > -1)
            this.users.splice(tmpUserIndex, 1);
          if (tmpUserFiltersIndex > -1)
            this.usersFilter.splice(tmpUserFiltersIndex, 1);
          user = null;
          this.modalRemoveUserRef.hide();
          this.toastr.success(this.sDic.getText('msg-success-exclude'));
        }, err => {
          this.sUtils.requestErrorHandler(err);
        });
    });

  }

}
