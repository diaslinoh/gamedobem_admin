import { Component, OnInit } from '@angular/core';
import { DictionaryService } from '../../services/dictionary.service';
import { NgForm } from '@angular/forms';
import { trigger, style, transition, animate } from '@angular/animations';
import { opacity } from "../../animations/opacity";
import { ApiService } from '../../services/api.service';
import { UserService } from '../../services/user.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  styleUrls: ['login.component.scss'],
  templateUrl: 'login.component.html',
  animations: [opacity]
})
export class LoginComponent implements OnInit {
  public errorMessage = null;
  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public sApi: ApiService,
    public sUser: UserService,
    public spinner: NgxSpinnerService
  ) {

  }
  ngOnInit() {
    // console.log(this.sUser.checkUserStatus());

    if (this.sUser.checkUserStatus()) {
      this.router.navigate(['/']);
      return;
    }
  }
  onSubmit(form: NgForm) {
    this.errorMessage = null;
    if (form.valid)
      this.doLogin(form.value);
    else
      this.errorMessage = this.sDic.getText('msg-form-invalid');

  }

  doLogin(data) {
    // this.spinner.show();

    this.sApi.doLogin(data)
      .subscribe(res => {

        console.log(res);
        this.sUser.setupUser(res['admin'], res['token']);
        this.router.navigate(['/']);
        // this.spinner.hide();
      }, err => {

        console.log(err);
        if (err['status'] == 500)
          this.errorMessage = this.sDic.getText('msg-server-error');
        else {
          this.errorMessage = this.sDic.getText('login-userpass-invalid');
        }
        // this.spinner.hide();
      })
  }
}
