
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { DictionaryService } from '../../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from '../../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RemoveConfirmComponent } from '../../../modals/remove-confirm/remove-confirm.component';
// import { timer } from "rxjs";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  public data: Array<any> = null;
  public dataFilter: Array<any> = null;
  public dataExtraany = null;

  public itemsPerPage = 10;
  public currentPage = 1;
  public searchChanged: Subject<string> = new Subject<string>();
  public modelFilters = { search: "" };


  public bsConfig = null;
  public modalRemoveUserRef: BsModalRef;



  constructor(
    private sApi: ApiService,
    public sDic: DictionaryService,
    public spinner: NgxSpinnerService,
    public sUtils: UtilsService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public modalService: BsModalService,
    public toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
    this.startData();
  }
  startData() {
    this.sApi.getProducts()
      .subscribe(res => {
        // console.log(res);
        this.data = res['data'];
        this.dataFilter = this.data.slice();
      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.data = undefined;
        this.dataFilter = undefined;
      });
  }

  doSearch(txt) {
    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(res => {
        this.doAllFilters();
      });
    this.searchChanged.next(txt);
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.dataFilter = this.dataFilter.filter(res => {
      let tmpNome = (res['nome'] ? res['nome'].toLowerCase() : "");
      tmpNome = tmpNome.replace(/  /gi, " ");
      let tmpEstado = (res['estado'] ? res['estado'].toLowerCase() : "");
      tmpEstado = tmpEstado.replace(/  /gi, " ");
      let tmpCidade = (res['cidade'] ? res['cidade'].toLowerCase() : "");
      tmpCidade = tmpCidade.replace(/  /gi, " ");
      let tmpPredio = (res['predio'] ? res['predio'].toLowerCase() : "");
      tmpPredio = tmpPredio.replace(/  /gi, " ");
      let tmpMoeda = (res['moeda'] ? (res['moeda']+"").toLowerCase() : "");
      tmpMoeda = tmpMoeda.replace(/  /gi, " ");
      return tmpNome.indexOf(ev) > -1 || tmpEstado.indexOf(ev) > -1 || tmpCidade.indexOf(ev) > -1 || tmpPredio.indexOf(ev) > -1 || tmpMoeda.indexOf(ev) > -1;
    });
  }
  filterPublished(published) {
    console.log(published);

    if (published == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      if (el.publicado == published)
        return true;
    });
  }

  doAllFilters() {
    this.currentPage = 1;
    setTimeout(() => {
      this.dataFilter = this.data.slice();
      this.filterSearch(this.modelFilters.search);
      // this.filterPublished(this.modelFilters.published);
    }, 100);
  }
  clearFilters() {
    this.modelFilters.search = "";
    // this.modelFilters.published = null;
    this.doAllFilters();
  }
  editData(data) {
    this.router.navigate(['update/' + data.id], { relativeTo: this.aRoute });
  }

  deleteData(data) {
    this.modalRemoveUserRef = this.modalService.show(RemoveConfirmComponent, { class: 'modal-dialog-centered' });
    this.modalRemoveUserRef.content.bsModalRef.content.onConfirm.subscribe(res => {
      console.log("confirmed: ", data);
      // this.sApi.deleteUser(data.id)
      this.spinner.show('spinner-extra');
      this.sApi.deleteProduct(data.id)
        .pipe(finalize(() => {
          this.spinner.hide('spinner-extra');
        }))
        .subscribe(res => {
          let tmpData = this.data.indexOf(data);
          let tmpDataFilter = this.dataFilter.indexOf(data);
          if (tmpData > -1)
            this.data.splice(tmpData, 1);
          if (tmpDataFilter > -1)
            this.dataFilter.splice(tmpDataFilter, 1);
          data = null;
          this.modalRemoveUserRef.hide();
          this.toastr.success(this.sDic.getText('msg-success-exclude'));
        }, err => {
          this.toastr.warning(this.sDic.getText('msg-not-possible-exclude-register'));
          // this.sUtils.requestErrorHandler(err);
        });
    });
    // let dataIndex = this.dataFilter.indexOf(data);
    // if (dataIndex > -1)
    //   this.dataFilter.splice(dataIndex, 1);
  }

  pageChanged(ev) {
  }
}
