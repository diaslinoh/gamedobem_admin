import { Component, OnInit, ViewChild } from '@angular/core';
import { DictionaryService } from '../../../../services/dictionary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { UtilsService } from '../../../../services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm, NgModel, ControlContainer, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { filter } from 'minimatch';

import urls from "../../../../../config/urls.json";

@Component({
  selector: 'app-products-update',
  templateUrl: './products-update.component.html',
  styleUrls: ['./products-update.component.scss']
})
export class ProductsUpdateComponent implements OnInit {
  @ViewChild('f', { static: false }) f: NgForm;
  public idData = null;
  public data = null;
  public dataExtra = null;

  public urlImgProduct = urls.urlBase + 'adm/loja_virtual/produtos/';
  public imgProduct = null;
  public imgUpload: File = null;
  public cities: Array<any> = null;
  public buildings: Array<any> = null;

  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public sApi: ApiService,
    public sUtils: UtilsService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idData = item.id;
    });
  }
  ngAfterViewInit() {
    this.startData();
    // console.log(this.f);
    setTimeout(() => {
      if (this.f) {
        this.initializeForm(this.f);
      }
    }, 0);

  }
  initializeForm(form: NgForm) {
    // form.controls['quantidade'].setValue(0);
  }
  fillForm(data) {
    this.f.controls['nome'].setValue(data['nome']);
    this.f.controls['quantidade'].setValue(data['quantidade']);
    this.f.controls['moeda'].setValue(data['moeda']);
    this.f.controls['descricao'].setValue(data['descricao']);
    this.f.controls['estado'].setValue(data['estado']);
    this.f.controls['imagem'].setValidators(null);
    this.f.controls['imagem'].setValue(null);


    // this.onStateChange(data['estado']);
    let tmpCities = this.onStateChange(data['estado']);
    if (data['estado'] && tmpCities.length > 0) {
      if (tmpCities.find(el => { return data['cidade'] == el.id })) {
        this.f.controls['cidade'].setValue(data['cidade']);
        let tmpBuildings = this.onCityChange(data['cidade']);
        if (tmpBuildings.find(el => { return data['id_predio'] == el.id })) {
          if (data['cidade'] && tmpBuildings.length > 0) {
            this.f.controls['predio'].setValue(data['id_predio']);
          }
        }

      }

    }
    if (data['imagem']) {
      this.imgProduct = this.urlImgProduct + data['imagem'];
    }


  }
  startData() {
    this.spinner.show('spinner-extra');
    this.sApi.getProductsExtra()
      .subscribe(res => {
        console.log(res);
        this.dataExtra = res;
        if (this.idData) {
          this.sApi.getProduct(this.idData)
            .subscribe(res2 => {
              console.log(res2);
              this.data = res2['data']['dados'];
              this.spinner.hide('spinner-extra');
              this.fillForm(this.data);
            }, err => {
              this.spinner.hide('spinner-extra');
              this.toastr.error(this.sDic.getText("common-data-not-loaded"))
              this.dataExtra = undefined;
              // this.router.navigate(['/products']);
            });
        } else {
          this.spinner.hide('spinner-extra');
        }
      }, err => {
        this.spinner.hide('spinner-extra');
        this.toastr.error(this.sDic.getText("common-data-not-loaded"));
        this.dataExtra = undefined;
        // this.router.navigate(['/products']);
      });
  }
  onSubmit(form: NgForm, e: Event) {
    e.preventDefault();
    // console.log(form);
    if (form.valid) {
      let formValue = Object.assign({}, form.value);

      let formData = new FormData();
      formData.append('nome', formValue.nome);
      formData.append('quantidade', formValue.quantidade);
      formData.append('moeda', formValue.moeda);
      if (formValue.descricao)
        formData.append('descricao', formValue.descricao);
      // formData.append('estado', formValue.estado);
      formData.append('estado', formValue.estado);
      formData.append('cidade', formValue.cidade);
      formData.append('predio', formValue.predio);
      formData.append('id_predio', formValue.predio);

      if (this.imgUpload)
        formData.append('imagem', this.imgUpload, this.imgUpload && this.imgUpload.name);

      if (this.idData) {
        formData.append('id', this.idData);
        console.log(formData);
        this.sApi.updateProduct(formData)
          .subscribe(res => {
            console.log(res);
            this.toastr.success(this.sDic.getText("msg-success-update-register"));
          }, err => {
            this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
            console.log(err);
          });
      } else {

        // console.log(formValue);

        this.sApi.addProduct(formData)
          .subscribe(res => {
            console.log(res);
            this.toastr.success(this.sDic.getText("msg-success-save-register"));
          }, err => {
            this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
            console.log(err);
          });
      }
    } else {
      this.toastr.warning(this.sDic.getText('msg-form-invalid'));
      let tmpInvalidField = document.querySelector(".ng-invalid:not(form):not(div)");
      if (tmpInvalidField) {
        tmpInvalidField.scrollIntoView();
        tmpInvalidField['focus']();
      }
    }

  }
  cutSize(size, el: NgModel) {
    // console.log(size, el);
    let tmpSub = new Subject();
    tmpSub
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(res => {
        el.control.setValue((el.value + "").replace(/\D/ig, '').substr(0, size));
      })
    tmpSub.next();

  }
  onImageUpload(e, model: NgModel) {


    let file: File = e.target.files[0];
    // console.log(file);
    if (file) {
      if ((file.type + "").match(/image/ig)) {
        // this.f.value.imagem = file;
        this.imgUpload = file;
        this.getBase64(file);
      } else {
        this.toastr.info(this.sDic.getText('common-not-image'));
        model.control.setValue(null);
        this.imgProduct = null;
        this.imgUpload = null;
      }
    } else {
      model.control.setValue(null);
      this.imgProduct = null;
      this.imgUpload = null;
    }

    // if (file && file['target'] && file['target']['files'] && file['target']['files'][0]) {
    //   this.getBase64(file['target']['files'][0]);
    // }
  }
  getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgProduct = reader.result;
      // console.log(reader.result);
    };
    reader.onerror = (error) => {
      console.log('Error: ', error);
    };
  }
  onStateChange(e) {
    let tmpValue = ((e && e.target && e.target.value) || e);
    // console.log(tmpValue);

    this.f.controls['cidade'].setValue(null);
    this.cities = this.dataExtra.cidades.filter(el => {
      return tmpValue == el['id_estado'];
    });
    return this.cities;

  }
  onCityChange(e) {
    let tmpValue = ((e && e.target && e.target.value) || e);
    this.f.controls['predio'].setValue(null);
    this.buildings = this.dataExtra.predio.filter(el => {
      return tmpValue == el['id_cidade'];
    });
    return this.buildings
  }
}
