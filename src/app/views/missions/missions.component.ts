
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DictionaryService } from '../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from '../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RemoveConfirmComponent } from '../../modals/remove-confirm/remove-confirm.component';
import { BsDatepickerInlineConfig } from 'ngx-bootstrap/datepicker/public_api';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.scss'],
  providers: [
    DatePipe
  ]
})
export class MissionsComponent implements OnInit {

  public data: Array<any> = null;
  public dataFilter: Array<any> = null;
  public dataExtra: any = null;
  // public dataExtraany = null;

  public itemsPerPage = 10;
  public currentPage = 1;
  public searchChanged: Subject<string> = new Subject<string>();
  public modelFilters = {
    "id_cat": null,
    "id_mecanica": null,
    "id_ods": null,
    "id_envolvimento": null,
    "fase": null,
    "nome": null,
    "descricao": null,
    "is_destaque": null,
    "is_gincana": null,
    "tags": null,
    "esp_titulo": null,
    "esp_descricao": null,
    "publicado": null,
    "publicado_em": null,
    "publicado_ate": null
  };


  public bsConfig = null;
  public modalRemoveUserRef: BsModalRef;



  constructor(
    private sApi: ApiService,
    public sDic: DictionaryService,
    public spinner: NgxSpinnerService,
    public sUtils: UtilsService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public modalService: BsModalService,
    public toastr: ToastrService,
    public pDate: DatePipe,
  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
    this.startData();
  }
  startData() {
    this.sApi.getMissions()
      .subscribe((res: Array<any>) => {
        this.sApi.getMissionExtra()
          .subscribe(res2 => {
            console.log(res);
            console.log(res2);
            this.data = res['data'];
            this.dataFilter = this.data.slice();
            this.dataExtra = res2['message'];
          }, err => {
            this.sUtils.requestErrorHandler(err);
            this.data = undefined;
            this.dataFilter = undefined;
          });

      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.data = undefined;
        this.dataFilter = undefined;
      });
  }

  doSearch(txt) {
    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(res => {
        this.doAllFilters();
      });
    this.searchChanged.next(txt);
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.dataFilter = this.dataFilter.filter(res => {
      let tmpNome = (res['nome'] ? res['nome'].toLowerCase() : "");
      tmpNome = tmpNome.replace(/  /gi, " ");
      let tmpOds = (res['nome_ods'] ? res['nome_ods'].toLowerCase() : "");
      tmpOds = tmpOds.replace(/  /gi, " ");

      return tmpNome.indexOf(ev) > -1 || tmpOds.indexOf(ev) > -1;
    });
  }
  filterPublished(published) {
    // console.log(published);

    if (published == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      if (el.publicado == published)
        return true;
    });
  }
  filterCategory(category) {
    // console.log(category);

    if (category == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      let currCategory = (el.categoria + "").toLowerCase().trim();
      if (currCategory == (category + "").toLowerCase().trim())
        return true;
    });
  }
  doFilterApi(data) {
    let tmpData = this.sUtils.mapObject(data);
    try {
      if (tmpData['publicado_em'])
        tmpData['publicado_em'] = this.pDate.transform(this.sUtils.dateToTimestamp(tmpData['publicado_em']), "yyyy-MM-dd HH:mm:ss");
      if (tmpData['publicado_ate'])
        tmpData['publicado_ate'] = this.pDate.transform(this.sUtils.dateToTimestamp(tmpData['publicado_ate']), "yyyy-MM-dd HH:mm:ss");
    } catch (error) {

    }


    console.log(tmpData);
    // console.log(this.sUtils.isDate(((new Date(data.publicado_em).getUTCDate()+""))));
    // console.log(this.sUtils.isDate(data.publicado_em, false));

    this.sApi.getMissionsFilter(tmpData)
      .subscribe(res => {
        console.log(res);
        this.currentPage = 1;
        setTimeout(() => {
          this.dataFilter = res['data'];
        }, 0);
      }, err => {
        // console.log(err);
        this.toastr.warning(this.sDic.getText("msg-not-possible-do-filter"));
      })
  }
  doAllFilters() {
    this.currentPage = 1;
    setTimeout(() => {
      this.dataFilter = this.data.slice();
      // this.filterSearch(this.modelFilters.search);
      // this.filterPublished(this.modelFilters.published);
      // this.filterCategory(this.modelFilters.category);
    }, 100);
  }
  clearFilters() {

    this.currentPage = 1;

    Object.keys(this.modelFilters).forEach(el => {
      this.modelFilters[el] = null;
    });
    this.dataFilter = this.data.slice();
    // this.doAllFilters();
  }
  editData(data) {
    this.router.navigate(['update/' + data.id_mis], { relativeTo: this.aRoute });
  }

  deleteData(data) {
    this.modalRemoveUserRef = this.modalService.show(RemoveConfirmComponent, { class: 'modal-dialog-centered' });
    this.modalRemoveUserRef.content.bsModalRef.content.onConfirm.subscribe(res => {
      
      // timer(2000)
      this.sApi.deleteMission(data.id_mis)
        .pipe(finalize(() => {
          this.spinner.hide('spinner-extra');
        }))
        .subscribe(res => {
          let tmpData = this.data.indexOf(data);
          let tmpDataFilter = this.dataFilter.indexOf(data);
          if (tmpData > -1)
            this.data.splice(tmpData, 1);
          if (tmpDataFilter > -1)
            this.dataFilter.splice(tmpDataFilter, 1);
          data = null;
          this.modalRemoveUserRef.hide();
          this.toastr.success(this.sDic.getText('msg-success-exclude'));
        }, err => {
          this.toastr.warning(this.sDic.getText('msg-not-possible-exclude-register'));
          // this.sUtils.requestErrorHandler(err);
        });
    });
    // let dataIndex = this.dataFilter.indexOf(data);
    // if (dataIndex > -1)
    //   this.dataFilter.splice(dataIndex, 1);
  }

  pageChanged(ev) {
  }

}
