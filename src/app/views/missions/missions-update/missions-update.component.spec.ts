import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsUpdateComponent } from './missions-update.component';

describe('MissionsUpdateComponent', () => {
  let component: MissionsUpdateComponent;
  let fixture: ComponentFixture<MissionsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
