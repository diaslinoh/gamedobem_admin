
import { Component, OnInit, ViewChild, ElementRef, Directive } from '@angular/core';
import { DictionaryService } from '../../../services/dictionary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { UtilsService } from '../../../services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm, NgModel, ControlContainer, FormControl, FormGroup, NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { Subject, throwError, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { filter } from 'minimatch';

import urls from "../../../../config/urls.json";
import { opacity } from '../../../animations/opacity';
import { trigger, transition, style, animate } from '@angular/animations';



/*
  MECÂNICAS
  id_mecania
  1 = IMAGEM
  2 = LINK
  3 = VIDEO
  4 = QUIZ
*/
@Component({
  selector: 'app-missions-update',
  templateUrl: './missions-update.component.html',
  styleUrls: ['./missions-update.component.scss'],
  animations: [
    trigger(
      'opacity', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('500ms', style({ opacity: 1 }))
      ])
      // , transition(':leave', [
      //   style({ opacity: 1 }),
      //   animate('500ms', style({ opacity: 0 }))
      // ])
    ]
    ),
    trigger(
      'scale', [
      transition(':enter', [
        style({ transform: "scale(0.5)" }),
        animate('500ms', style({ transform: "scale(1)" }))
      ])
      , transition(':leave', [
        style({ transform: "scale(1)" }),
        animate('500ms', style({ transform: "scale(0)" }))
      ])
    ]
    )
  ]
})
export class MissionsUpdateComponent implements OnInit {




  @ViewChild('f', { static: false }) f: NgForm;
  public idData = null;
  public data = null;
  public dataExtra = null;

  public bsConfig = null;

  public video: File = null;
  public videoPreview = null;

  // public totPerg = [1, 1];
  // public totAns = 3;
  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public sApi: ApiService,
    public sUtils: UtilsService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
  ) {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
  }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idData = item.id;
    });
  }
  ngAfterViewInit() {
    this.startData();
    // console.log(this.f);
    setTimeout(() => {
      if (this.f) {
        this.initializeForm(this.f);
      }
    }, 0);

  }
  initializeForm(form: NgForm) {
    if (form.controls['passos']) {
      form.controls['passos'].setValidators([(validator: FormGroup) => {
        let tmp = Object.keys(validator.value).find(el => {
          if (validator.value[el]) {
            return true;
          }
        });
        if (!tmp) {
          form.controls['passos'].setErrors({ 'require_one': "Require one" });
          return { require_one: "Require one" };
        }
        return null;
      }, (validator: FormGroup) => {
        let tot = 0;
        Object.keys(validator.value).forEach(el => {
          if (validator.value[el]) {
            tot = tot + 1;
          }
        });
        if (tot > 5) {
          form.controls['passos'].setErrors({ 'max': true });
          return { 'max': true };
        }
        return null;
      }]);
    }
    // form.controls['quantidade'].setValue(0);
  }
  fillForm(data) {
    console.log(data);
    let tmpData = Object.assign({}, data);

    let tmpPerguntas;
    if (Array.isArray(tmpData['missao'])) {
      tmpPerguntas = tmpData['missao'][1];
      tmpData['missao'] = tmpData['missao'][0];
      // console.log(tmpData);

    }
    this.f.controls['nome'].setValue(tmpData['missao']['nome']);
    this.f.controls['id_cat'].setValue(tmpData['missao']['id_cat']);
    this.f.controls['descricao_curta'].setValue(tmpData['missao']['descricao_curta']);
    this.f.controls['descricao'].setValue(tmpData['missao']['descricao']);
    this.f.controls['pontos'].setValue(tmpData['missao']['pontos']);
    this.f.controls['moeda'].setValue(tmpData['missao']['moeda']);
    this.f.controls['horas_voluntariado'].setValue(tmpData['missao']['horas_voluntariado']);
    this.f.controls['id_ods'].setValue(tmpData['missao']['id_ods']);
    this.f.controls['id_beneficiado'].setValue(tmpData['missao']['id_beneficiado']);
    this.f.controls['id_envolvimento'].setValue(tmpData['missao']['id_envolvimento']);
    this.f.controls['bonus_tipo'].setValue(tmpData['missao']['bonus_tipo']);



    Object.keys(tmpData['missao']).forEach(el => {
      if ((el + "").toLowerCase().indexOf('passo') > -1) {
        this.dataExtra.passos.forEach(el2 => {
          // console.log((data['missao'][el] + "").toLowerCase());

          if ((el2.passos + "").toLowerCase() == (tmpData['missao'][el] + "").toLowerCase()) {
            // console.log(el2);

            Object.keys(this.f.controls['passos'].value).find((el3) => {
              if ((el3 + "").split("_")[1] == (el2.id + "")) {
                // console.log(el3);
                this.f.controls['passos']['controls'][el3].setValue(true);
                return true;
              }
            })
          }
        });
      }
    });
    this.f.controls['id_mecanica'].setValue(tmpData['missao']['id_mecanica']);
    // this.f.controls['id_mecanica'].setValue(4);

    setTimeout(() => {
      if (this.f.controls['id_mecanica'].value == 4) {
        let tmpQuestions: Array<any> = data['missao'][1];
        if (tmpQuestions) {
          setTimeout(() => {
            this.f.controls['quizzes_titulo'].setValue(data['missao'][0]['quizzes']['title']);
            this.f.controls['quizzes_description'].setValue(data['missao'][0]['quizzes']['description']);
            this.f.controls['quiz_categoria_id'].setValue(data['missao'][1][0]['question']['category_id']);
            this.f.controls['perguntas']['controls']['totQuestion'].setValue(tmpQuestions.length);
            setTimeout(() => {
              tmpQuestions.forEach((el, idx) => {
                let currQuestion = this.f.controls['perguntas']['controls']['pergunta_' + idx];
                currQuestion['controls']['totAnswer'].setValue(el.alternatives.length);
                currQuestion['controls']['pergunta'].setValue(el.question.title);
                currQuestion['controls']['desc_pergunta'].setValue(el.question.description);
                currQuestion['controls']['pontos'].setValue(el.question.points);
                setTimeout(() => {
                  let tmpAnswers = [];
                  let tmpCorrect = null;
                  el.alternatives.forEach((el2, idx2) => {
                    let currAnwser: FormControl = currQuestion['controls']['respostas']['controls']['resposta_' + idx2];
                    currQuestion['controls']['respostas']['controls']['resposta_' + idx2].setValue(el2.question);
                    // currQuestion['controls']['respostas']['controls']['correct_' + idx2].setValue(el2.correct == 1 ? true : false);
                  });
                  // tmpCorrect = el.alternatives.find((el2, idx2) => {
                  //   if (el2.correct) {
                  //     return true;                      
                  //   }
                  // });
                  // if (tmpCorrect) {
                  //   tmpAnswers.push(tmpCorrect.question);
                  // } else {
                  //   tmpAnswers.push(null);
                  // }

                  // el.alternatives.forEach((el2, idx2) => {
                  //   if (!el2.correct) {
                  //     tmpAnswers.push(el2.question);
                  //   }
                  // });

                  // tmpAnswers = tmpAnswers.slice(0, el.alternatives.length);


                  // tmpAnswers.forEach((el3, idx3) => {


                  //   let currAnwser: FormControl = currQuestion['controls']['respostas']['controls']['resposta_' + idx3];
                  //   currQuestion['controls']['respostas']['controls']['resposta_' + idx3].setValue(el3);
                  // });
                }, 0);
              });
              // this.f.controls['perguntas']['controls']['pergunta_1']['controls']['totAnswer'].setValue(5);
            }, 0);

          }, 0);
        }


      }

      if (this.f.controls['id_mecanica'].value == 2) {
        this.f.controls['bonus_url'].setValue(tmpData['missao']['bonus_url']);
      }
      if (this.f.controls['id_mecanica'].value == 3) {
        this.f.controls['bonus_url'].setValue(tmpData['missao']['bonus_url']);
      }

    }, 0);

    // this.f.controls['pergunta'].setValue(data['pergunta']);
    // this.f.controls['resposta'].setValue(data['resposta']);
    // this.f.controls['id_cat'].setValue(data['id_cat']);
    // this.f.controls['publicado'].setValue(data['publicado']);

    // if (this.sUtils.isDate(data['publicado_em'])) {
    //   this.f.controls['publicado_em'].setValue(new Date(this.sUtils.getDateTimestamp(data['publicado_em'])));
    // }
    // if (this.sUtils.isDate(data['publicado_ate'])) {
    //   this.f.controls['publicado_ate'].setValue(new Date(this.sUtils.getDateTimestamp(data['publicado_ate'])));
    // }

  }
  startData() {
    this.sApi.getMissionExtra()
      .subscribe(res => {
        console.log(res);
        this.dataExtra = res['message'];

        if (this.idData) {
          this.sApi.getMission(this.idData)
            .pipe(tap((x) => {
              // console.log(x);
              // if (x && x['data'] && x['data']['dados']) {

              // } else {
              //   throw Observable.throw('Oops!');
              // }
            }))
            .subscribe(res2 => {
              console.log(res2);
              // this.dataExtra = res['data'];
              this.data = res2['message'];
              this.fillForm(this.data);
            }, err => {
              console.log(err);
              this.toastr.error(this.sDic.getText("common-data-not-loaded"))
              this.dataExtra = undefined;
            })
        } else {

        }
      }, err => {
        console.log(err);
        this.toastr.error(this.sDic.getText("common-data-not-loaded"))
        this.dataExtra = undefined;

      });


  }
  onSubmit(form: NgForm, e: Event) {
    e.preventDefault();
    console.log(form);

    let formValue = Object.assign({}, form.value);
    let formData = new FormData();
    let tmpData = {};

    tmpData["nome"] = formValue.nome;
    tmpData["descricao"] = formValue.descricao;
    tmpData["descricao_curta"] = formValue.descricao_curta;
    tmpData["pontos"] = formValue.pontos;
    tmpData["id_mecanica"] = formValue.id_mecanica;
    tmpData["moeda"] = formValue.moeda;
    tmpData["id_ods"] = formValue.id_ods;
    tmpData["id_beneficiado"] = formValue.id_beneficiado;
    tmpData["id_envolvimento"] = formValue.id_envolvimento;
    tmpData["id_cat"] = formValue.id_cat;
    tmpData["horas_voluntariado"] = formValue.horas_voluntariado;
    tmpData["bonus_tipo"] = formValue.bonus_tipo;

    tmpData["curiosidade"] = formValue.curiosidade;
    tmpData["tags"] = formValue.tags;
    tmpData["publicado"] = formValue.tags;


    let tmpPassos = Object.keys(formValue.passos).map(el => {
      if (formValue.passos[el]) {
        return parseInt((el + "").split('_')[1]);
      }
      else
        return null;
    }).filter(el2 => {
      return el2 != null;
    });
    tmpData["passos"] = tmpPassos;


    if (formValue.id_mecanica == 4) {
      tmpData["quiz_categoria_id"] = formValue.quiz_categoria_id;
      tmpData["quizzes_titulo"] = formValue.quizzes_titulo;
      tmpData["quizzes_description"] = formValue.quizzes_description;

      // formData.append("quiz_categoria_id", formValue.quiz_categoria_id);
      // formData.append("quizzes_titulo", formValue.quizzes_titulo);
      // formData.append("quizzes_description", formValue.quizzes_description);

      let tmpQuestions = [];
      let perguntas = formValue.perguntas || {};
      Object.keys(perguntas).forEach(el => {

        if ((el + "").indexOf('pergunta') > -1) {
          let tmpQ = [];
          let currPergunta = perguntas[el];
          tmpQ.push(currPergunta['pergunta']);
          tmpQ.push(currPergunta['desc_pergunta']);
          tmpQ.push(currPergunta['pontos']);

          Object.keys(currPergunta['respostas']).forEach((el2, index) => {
            if ((el2 + "").indexOf('resposta') > -1) {
              tmpQ.push(currPergunta['respostas'][el2]);
            }
          });
          Object.keys(currPergunta['respostas']).forEach((el2, index) => {
            if ((el2 + "").indexOf('resposta') > -1) {
              if (index == 0)
                tmpQ.push(1);
              else {
                tmpQ.push(0);
              }
            }
            // if ((el2 + "").indexOf('correct') > -1) {
            //   if (currPergunta['respostas'][el2]) {
            //     tmpQ.push(1);
            //   } else {
            //     tmpQ.push(0);
            //   }
            // }

          });
          // Object.keys(currPergunta['respostas']).forEach((el2, index) => {
          //   if ((el2 + "").indexOf('resposta') > -1) {
          //     tmpQ.push(currPergunta['respostas'][el2]);
          //   }
          // });
          // Object.keys(currPergunta['respostas']).forEach((el2, index) => {
          //   if ((el2 + "").indexOf('resposta') > -1) {
          //     if (index == 0) {
          //       tmpQ.push(1);
          //     } else {
          //       tmpQ.push(0);
          //     }
          //   }

          // });
          tmpQuestions.push(tmpQ);
        }
      });
      tmpData["questions"] = tmpQuestions;
      // tmpData["quizzes_number_of_questions"] = tmpQuestions.length;
      // formData.append("questions", JSON.stringify(tmpQuestions));
      // formData.append("quizzes_number_of_questions", (tmpQuestions.length + ""));


    } else if (formValue.id_mecanica == 1) {
      tmpData['comp_imagem'] = 1;
    } else if (formValue.id_mecanica == 2) {
      tmpData["bonus_url"] = formValue.bonus_url;
      // formData.append("bonus_url", formValue.bonus_url);

    } else if (formValue.id_mecanica == 3) {
      tmpData['comp_video'] = 1;
      tmpData["bonus_url"] = this.video;
      try {
        formData.append("nome", formValue.nome);
        formData.append("descricao", formValue.descricao);
        formData.append("descricao_curta", formValue.descricao_curta);
        formData.append("pontos", formValue.pontos);
        formData.append("id_mecanica", formValue.id_mecanica);
        formData.append("moeda", formValue.moeda);
        formData.append("id_ods", formValue.id_ods);
        formData.append("id_beneficiado", formValue.id_beneficiado);
        formData.append("id_envolvimento", formValue.id_envolvimento);
        formData.append("id_cat", formValue.id_cat);
        formData.append("horas_voluntariado", formValue.horas_voluntariado);
        formData.append("bonus_tipo", formValue.bonus_tipo);
        formData.append("comp_video", "1");
        formData.append("passos", JSON.stringify(tmpPassos));
        if (this.video)
          formData.append("bonus_url", this.video, ((this.video && this.video.name) || ''));
        console.log(formData.getAll('nome'));

      } catch (error) {

      }

    }

    console.log(formValue);
    console.log(tmpData);


    if (form.valid) {

      // formValue.publicado_em = this.sUtils.dateToTimestamp(formValue.publicado_em, true);
      // formValue.publicado = formValue.publicado == 1 ? true : false;
      // if (!formValue.publicado_ate)
      //   delete formValue.publicado_ate;
      // else
      //   formValue.publicado_ate = this.sUtils.dateToTimestamp(formValue.publicado_ate, true);
      // console.log(formValue);
      let dataToSend;
      if (this.idData) {
        tmpData['id_mis'] = this.idData;

        if (formValue.id_mecanica == 3) {
          formData.append("id_mis", tmpData['id_mis']);
          dataToSend = formData;
        } else {
          dataToSend = tmpData;
        }

        // this.sApi.updateMission(dataToSend)
        //   .subscribe(res => {
        //     this.toastr.success(this.sDic.getText("msg-success-update-register"));
        //   }, err => {
        //     console.log(err);
        //     this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
        //   });
      } else {
        if (formValue.id_mecanica == 3) {
          dataToSend = formData;
        } else {
          dataToSend = tmpData;
        }
        // this.sApi.addMission(dataToSend)
        //   .subscribe(res => {
        //     this.toastr.success(this.sDic.getText("msg-success-save-register"));
        //   }, err => {
        //     console.log(err);
        //     this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
        //   });


        // this.sApi.addFaq(formValue)
        //   .subscribe(res => {
        //     this.toastr.success(this.sDic.getText("msg-success-save-register"));
        //   }, err => {
        //     console.log(err);
        //     this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
        //   });
      }
    } else {
      this.toastr.warning(this.sDic.getText('msg-form-invalid'));
      let tmpInvalidField = document.querySelector(".ng-invalid:not(form):not(div)");
      if (tmpInvalidField) {
        tmpInvalidField.scrollIntoView();
        tmpInvalidField['focus']();
      }
    }

  }
  cutSize(size, el: NgModel) {
    // console.log(size, el);
    let tmpSub = new Subject();
    tmpSub
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(res => {
        el.control.setValue((el.value + "").replace(/\D/ig, '').substr(0, size));
      })
    tmpSub.next();

  }
  // addQuestion(val) {
  //   val.value = parseInt(val.value) + 1;
  // }
  // removeQuestion(val) {
  //   let tmpValue = parseInt(val.value);
  //   if (tmpValue <= 1)
  //     return;
  //   val.value = parseInt(val.value) - 1;
  // }
  addQuestion(val: NgModel) {
    console.log(typeof val);
    val.control.setValue(parseInt(val.value) + 1);
  }
  removeQuestion(val: NgModel) {
    let tmpValue = parseInt(val.value);
    if (tmpValue <= 1)
      return;
    val.control.setValue(parseInt(val.value) - 1);
  }
  addAnswer(val: NgModel) {
    console.log(typeof val);
    val.control.setValue(parseInt(val.value) + 1);
  }
  removeAnswer(val: NgModel) {
    let tmpValue = parseInt(val.value);
    if (tmpValue <= 2)
      return;
    val.control.setValue(parseInt(val.value) - 1);
  }

  onVideoChange(e, model: NgModel) {

    let tmpFile: File = e['target']['files'][0];
    console.log(tmpFile);
    if (tmpFile) {
      if (/video/ig.test(tmpFile.type.toLocaleLowerCase())) {
        this.video = tmpFile;

        var reader = new FileReader();

        reader.onload = (e) => {
          this.videoPreview = e['target']['result'];

          setTimeout(() => {
            let vPreview = document.querySelector('.video-preview');
            vPreview['onloadeddata'] = (e) => {
              // console.log(e);              
              vPreview.scrollIntoView(false);
            }


          }, 200);
          // $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(tmpFile);


      } else {
        this.toastr.warning(this.sDic.getText('mission-not-video'));
        this.video = null;
        this.videoPreview = null;
        model.control.setValue(null);
      }
    } else {
      this.video = null;
      this.videoPreview = null;
      model.control.setValue(null);
    }

  }
}

