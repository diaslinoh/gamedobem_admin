
import { Component, OnInit, ViewChild } from '@angular/core';
import { DictionaryService } from '../../../services/dictionary.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { UtilsService } from '../../../services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm, NgModel, ControlContainer, FormControl } from '@angular/forms';
import { Subject, throwError, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { filter } from 'minimatch';

import urls from "../../../../config/urls.json";


@Component({
  selector: 'app-faq-update',
  templateUrl: './faq-update.component.html',
  styleUrls: ['./faq-update.component.scss']
})
export class FaqUpdateComponent implements OnInit {


  @ViewChild('f', { static: false }) f: NgForm;
  public idData = null;
  public data = null;
  public dataExtra = null;

  public bsConfig = null;

  constructor(
    public sDic: DictionaryService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public sApi: ApiService,
    public sUtils: UtilsService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
  ) {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
  }

  ngOnInit() {
    this.aRoute.params.forEach((item) => {
      this.idData = item.id;
    });
  }
  ngAfterViewInit() {
    this.startData();
    // console.log(this.f);
    setTimeout(() => {
      if (this.f) {
        this.initializeForm(this.f);
      }
    }, 0);

  }
  initializeForm(form: NgForm) {
    // form.controls['quantidade'].setValue(0);
  }
  fillForm(data) {
    this.f.controls['pergunta'].setValue(data['pergunta']);
    this.f.controls['resposta'].setValue(data['resposta']);
    this.f.controls['id_cat'].setValue(data['id_cat']);
    // setTimeout(() => {
    this.f.controls['publicado'].setValue(data['publicado']);
    // }, 0);
    // this.f.controls['publicado_em'].setValue(new Date(this.sUtils.getDateTimestamp(data['publicado_em'])));
    // let tmpPublicadoEm = this.sUtils.getDateTimestamp("0000-00-00 00:00:00");
    // console.log(tmpPublicadoEm);
    if (this.sUtils.isDate(data['publicado_em'])) {
      this.f.controls['publicado_em'].setValue(new Date(this.sUtils.getDateTimestamp(data['publicado_em'])));
    }
    if (this.sUtils.isDate(data['publicado_ate'])) {
      this.f.controls['publicado_ate'].setValue(new Date(this.sUtils.getDateTimestamp(data['publicado_ate'])));
    }

  }
  startData() {
    this.sApi.getFaqExtra()
      .subscribe(res => {
        console.log(res);
        this.dataExtra = res['data'];
        if (this.idData) {
          this.sApi.getFaq(this.idData)
            .pipe(tap((x) => {
              console.log(x);
              if (x && x['data'] && x['data']['dados']) {

              } else {
                // throwError("Oops!");
                throw Observable.throw('Oops!');
              }
            }))
            .subscribe(res2 => {
              console.log(res2);
              // this.dataExtra = res['data'];
              this.data = res2['data']['dados'];
              this.fillForm(this.data);
            }, err => {
              console.log(err);
              this.toastr.error(this.sDic.getText("common-data-not-loaded"))
              this.dataExtra = undefined;
            })
        } else {

        }
      }, err => {
        console.log(err);
        this.toastr.error(this.sDic.getText("common-data-not-loaded"))
        this.dataExtra = undefined;

      });
    // if (this.idData) {
    //   this.sApi.getCompanie(this.idData)
    //     .subscribe(res2 => {
    //       console.log(res2);
    //       this.data = res2[0];
    //       // this.spinner.hide('spinner-extra');
    //       this.fillForm(this.data);
    //     }, err => {
    //       // this.spinner.hide('spinner-extra');
    //       this.toastr.error(this.sDic.getText("common-data-not-loaded"))
    //       this.dataExtra = undefined;
    //       // this.router.navigate(['/products']);
    //     });
    // }

  }
  onSubmit(form: NgForm, e: Event) {
    e.preventDefault();
    console.log(form);
    if (form.valid) {
      let formValue = Object.assign({}, form.value);
      formValue.publicado_em = this.sUtils.dateToTimestamp(formValue.publicado_em, true);
      formValue.publicado = formValue.publicado == 1 ? true : false;
      if (!formValue.publicado_ate)
        delete formValue.publicado_ate;
      else
        formValue.publicado_ate = this.sUtils.dateToTimestamp(formValue.publicado_ate, true);
      console.log(formValue);

      if (this.idData) {
        formValue.id_faq = this.idData;
        this.sApi.updateFaq(formValue)
          .subscribe(res => {
            this.toastr.success(this.sDic.getText("msg-success-update-register"));
          }, err => {
            console.log(err);
            this.toastr.warning(this.sDic.getText('msg-not-possible-update-register'));
          });
      } else {
        this.sApi.addFaq(formValue)
          .subscribe(res => {
            this.toastr.success(this.sDic.getText("msg-success-save-register"));
          }, err => {
            console.log(err);
            this.toastr.warning(this.sDic.getText('msg-not-possible-save-register'));
          });
      }
    } else {
      this.toastr.warning(this.sDic.getText('msg-form-invalid'));
      let tmpInvalidField = document.querySelector(".ng-invalid:not(form)");
      if (tmpInvalidField) {
        tmpInvalidField.scrollIntoView();
        tmpInvalidField['focus']();
      }
    }

  }
  cutSize(size, el: NgModel) {
    // console.log(size, el);
    let tmpSub = new Subject();
    tmpSub
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(res => {
        el.control.setValue((el.value + "").replace(/\D/ig, '').substr(0, size));
      })
    tmpSub.next();

  }
}
