
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DictionaryService } from '../../services/dictionary.service';
import { Subject, timer } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from '../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { RemoveConfirmComponent } from '../../modals/remove-confirm/remove-confirm.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  public data: Array<any> = null;
  public dataFilter: Array<any> = null;
  public dataExtra: any = null;
  // public dataExtraany = null;

  public itemsPerPage = 10;
  public currentPage = 1;
  public searchChanged: Subject<string> = new Subject<string>();
  public modelFilters = { search: "", published: null, category: null };


  public bsConfig = null;
  public modalRemoveUserRef: BsModalRef;



  constructor(
    private sApi: ApiService,
    public sDic: DictionaryService,
    public spinner: NgxSpinnerService,
    public sUtils: UtilsService,
    public router: Router,
    public aRoute: ActivatedRoute,
    public modalService: BsModalService,
    public toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.bsConfig = { dateInputFormat: 'DD/MM/YYYY', adaptivePosition: true, containerClass: 'theme-default' };
    this.startData();
  }
  startData() {
    this.sApi.getFaqs()
      .subscribe((res: Array<any>) => {
        this.sApi.getFaqExtra()
          .subscribe(res2 => {
            console.log(res);
            // console.log(res2);
            this.data = res;
            this.dataFilter = this.data.slice();
            this.dataExtra = res2['data'];
          }, err => {
            this.sUtils.requestErrorHandler(err);
            this.data = undefined;
            this.dataFilter = undefined;
          });

      }, err => {
        this.sUtils.requestErrorHandler(err);
        this.data = undefined;
        this.dataFilter = undefined;
      });
  }

  doSearch(txt) {
    this.searchChanged.pipe(debounceTime(300), distinctUntilChanged())
      .subscribe(res => {
        this.doAllFilters();
      });
    this.searchChanged.next(txt);
  }

  filterSearch(ev: string) {
    ev = ev.toLowerCase().trim();
    this.dataFilter = this.dataFilter.filter(res => {
      let tmpPergunta = (res['pergunta'] ? res['pergunta'].toLowerCase() : "");
      tmpPergunta = tmpPergunta.replace(/  /gi, " ");
      let tmpCategoria = (res['categoria'] ? res['categoria'].toLowerCase() : "");
      tmpCategoria = tmpCategoria.replace(/  /gi, " ");

      return tmpPergunta.indexOf(ev) > -1 || tmpCategoria.indexOf(ev) > -1;
    });
  }
  filterPublished(published) {
    console.log(published);

    if (published == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      if (el.publicado == published)
        return true;
    });
  }
  filterCategory(category) {
    // console.log(category);

    if (category == null)
      return;
    this.dataFilter = this.dataFilter.filter(el => {
      let currCategory = (el.categoria + "").toLowerCase().trim();
      if (currCategory == (category + "").toLowerCase().trim())
        return true;
    });
  }

  doAllFilters() {
    this.currentPage = 1;
    setTimeout(() => {
      this.dataFilter = this.data.slice();
      this.filterSearch(this.modelFilters.search);
      this.filterPublished(this.modelFilters.published);
      this.filterCategory(this.modelFilters.category);
    }, 100);
  }
  clearFilters() {
    this.modelFilters.search = "";
    this.modelFilters.published = null;
    this.modelFilters.category = null;
    this.doAllFilters();
  }
  editData(data) {
    this.router.navigate(['update/' + data.id_faq], { relativeTo: this.aRoute });
  }

  deleteData(data) {
    this.modalRemoveUserRef = this.modalService.show(RemoveConfirmComponent, { class: 'modal-dialog-centered' });
    this.modalRemoveUserRef.content.bsModalRef.content.onConfirm.subscribe(res => {
      console.log("confirmed: ", data);
      // this.sApi.deleteUser(data.id)
      this.spinner.show('spinner-extra');
      // timer(2000)
      this.sApi.deleteFaq(data.id_faq)
        .pipe(finalize(() => {
          this.spinner.hide('spinner-extra');
        }))
        .subscribe(res => {
          let tmpData = this.data.indexOf(data);
          let tmpDataFilter = this.dataFilter.indexOf(data);
          if (tmpData > -1)
            this.data.splice(tmpData, 1);
          if (tmpDataFilter > -1)
            this.dataFilter.splice(tmpDataFilter, 1);
          data = null;
          this.modalRemoveUserRef.hide();
          this.toastr.success(this.sDic.getText('msg-success-exclude'));
        }, err => {
          this.sUtils.requestErrorHandler(err);
        });
    });
    // let dataIndex = this.dataFilter.indexOf(data);
    // if (dataIndex > -1)
    //   this.dataFilter.splice(dataIndex, 1);
  }

  pageChanged(ev) {
  }

}
