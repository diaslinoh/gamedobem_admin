import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTimestamp'
})
export class DateTimestampPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    try {
      let tmpDate = (value as string).split(' ')[0];
      let tmpTime = (value as string).split(' ')[1];

      let day = parseInt(tmpDate.split('-')[2]);
      let mounth = (parseInt(tmpDate.split('-')[1]) - 1) < 0 ? 0 : (parseInt(tmpDate.split('-')[1]) - 1);
      let year = parseInt(tmpDate.split('-')[0]);
      let hour = parseInt(tmpTime.split(':')[0]);
      let minute = parseInt(tmpTime.split(':')[1]);
      let second = parseInt(tmpTime.split(':')[2]);

      let d = new Date(year, mounth, day, hour, minute, second);
      return d.getTime();
    } catch (error) {
      return null;
    }

  }

}
