import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { TestComponent } from './views/test/test.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { UserComponent } from './views/user/user.component';
import { EmployeesComponent } from './views/employees/employees.component';
import { EmployeeUpdateComponent } from './views/employees/employee-update/employee-update.component';
import { UserUpdateComponent } from './views/user/user-update/user-update.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { CompanyComponent } from './views/company/company.component';
import { ProductsComponent } from './views/store/products/products.component';
import { ProductsUpdateComponent } from './views/store/products/products-update/products-update.component';
import { DictionaryService } from './services/dictionary.service';
import { CompanyUpdateComponent } from './views/company/company-update/company-update.component';
import { FaqComponent } from './views/faq/faq.component';
import { FaqUpdateComponent } from './views/faq/faq-update/faq-update.component';
import { MissionsComponent } from './views/missions/missions.component';
import { MissionsUpdateComponent } from './views/missions/missions-update/missions-update.component';
import { MissionsRealComponent } from './views/missions-real/missions-real.component';
import { MissionsRealUpdateComponent } from './views/missions-real/missions-real-update/missions-real-update.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  // {
  //   path: '404',
  //   component: P404Component,
  //   data: {
  //     title: 'Page 404'
  //   }
  // },
  // {
  //   path: '500',
  //   component: P500Component,
  //   data: {
  //     title: 'Page 500'
  //   }
  // },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: DictionaryService.getInstance().getText('common-login')
    }
  },
  // {
  //   path: 'register',
  //   component: RegisterComponent,
  //   data: {
  //     title: 'Register Page'
  //   }
  // },

  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService],
    children: [
      {
        path: 'dashboard',
        component: TestComponent,
        // canActivate: [AuthGuardService],
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'test',
        component: TestComponent,
        // canActivate: [AuthGuardService],
        data: {
          title: 'Test'
        }
      },
      {
        path: 'users',

        data: {
          title: DictionaryService.getInstance().getText('common-users')
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: { title: '' },
                component: UserComponent
              },
              {
                path: 'add',
                component: UserUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('users-add'),
                  action: "update"
                }

              },
              {
                path: 'update/:id',
                component: UserUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('users-edit'),
                  action: "update"
                }

              },
            ]
          }
        ]
      },
      {
        path: 'employees',
        // component: EmployeesComponent,
        data: {
          title: DictionaryService.getInstance().getText('common-employees'),
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                component: EmployeesComponent,
                data: {
                  title: ""
                }
              },
              {
                path: 'update/:id',
                component: EmployeeUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('employees-edit'),
                  action: "update"
                }
              },
              {
                path: 'add',
                component: EmployeeUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('employees-add'),
                  action: "add"
                }
              }
            ]
          }
        ]
      },
      {
        path: 'company',
        data: {
          title: DictionaryService.getInstance().getText('common-companies'),
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: {
                  title: ''
                },
                component: CompanyComponent
              },
              {
                path: 'update/:id',
                component: CompanyUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('company-edit'),
                  action: "update"
                }
              },
              {
                path: 'add',
                component: CompanyUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('company-add'),
                  action: "add"
                }
              }
            ]
          }
        ]
      },
      {
        path: 'products',
        data: {
          title: DictionaryService.getInstance().getText('common-products'),
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: { title: '' },
                component: ProductsComponent,
              },
              {
                path: 'add',
                component: ProductsUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('product-add'),
                  action: "add"
                }
              },
              {
                path: 'update/:id',
                component: ProductsUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('product-edit'),
                  action: "edit"
                }
              }
            ]
          }

        ]
      },
      {
        path: "faq",
        data: {
          title: DictionaryService.getInstance().getText('common-faq')
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: { title: '' },
                component: FaqComponent,
              },
              {
                path: 'add',
                component: FaqUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('faq-add'),
                  action: "add"
                }
              },
              {
                path: 'update/:id',
                component: FaqUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('faq-edit'),
                  action: "edit"
                }
              }
            ]
          }
        ]
      },
      {
        path: "missions",
        data: {
          title: DictionaryService.getInstance().getText('common-missions')
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: { title: '' },
                component: MissionsComponent,
              },
              {
                path: 'add',
                component: MissionsUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('mission-add'),
                  action: "add"
                }
              },
              {
                path: 'update/:id',
                component: MissionsUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('mission-edit'),
                  action: "edit"
                }
              }
            ]
          }
        ]
      },
      {
        path: "realized",
        data: {
          title: DictionaryService.getInstance().getText('mission-real')
        },
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                data: { title: '' },
                component: MissionsRealComponent,
              },
              {
                path: 'update/:id',
                component: MissionsRealUpdateComponent,
                data: {
                  title: DictionaryService.getInstance().getText('mission-real-view'),
                  action: "view"
                }
              }
            ]
          }
        ]
      }
      // {
      //   path: 'base',
      //   loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      // },
      // {
      //   path: 'buttons',
      //   loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      // },
      // {
      //   path: 'charts',
      //   loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      // },
      // {
      //   path: 'dashboard',
      //   loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      // },
      // {
      //   path: 'icons',
      //   loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      // },
      // {
      //   path: 'notifications',
      //   loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      // },
      // {
      //   path: 'theme',
      //   loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      // },
      // {
      //   path: 'widgets',
      //   loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      // }
    ]
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
