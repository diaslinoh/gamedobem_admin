import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ResolveEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DictionaryService } from './services/dictionary.service';
import { HttpClient } from '@angular/common/http';


@Component({
  // tslint:disable-next-line
  selector: 'body',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private sDic: DictionaryService,
    private http: HttpClient,
    private translate: TranslateService) {
    // translate.setDefaultLang('en');
  }

  ngOnInit() {
   
    // this.http.post('http://apigamedobem.kinterativa.com/adm/sugest/list', {})
    //   .subscribe(res => {
    //     console.log(res);

    //   }, err => {
    //     console.log(err);
    //   });

    this.router.events.subscribe((evt) => {
      // if (evt instanceof ResolveEnd)
      //   console.log(evt);

      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      setTimeout(() => {
        window.scrollTo(0, 0);
      }, 200);
    });
  }
}
