import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
// import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';



// MY IMPORT
import { NgxMaskModule, IConfig } from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';

// import ngx-translate and the http loader
import { PaginationModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';


import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { BsDatepickerModule } from 'ngx-bootstrap';


// Custom Imports ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// GUARD
import { AuthGuardService } from "./guards/auth-guard.service";

// SERVICES
import { DictionaryService } from './services/dictionary.service';
import { ApiService } from "./services/api.service";
import { UtilsService } from "./services/utils.service";
import { UserService } from "./services/user.service";
import { UsersCommonService } from "./views/user/users-common.service";


// COMPONENTS
import { TestComponent } from './views/test/test.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { Commonterceptor } from './interceptors/common.interceptor';
import { UserComponent } from './views/user/user.component';
import { EmployeesComponent } from "./views/employees/employees.component";
// import { UserDetailComponent } from './views/user/user-detail/user-detail.component';
import { DateTimestampPipe } from './pipes/date-timestamp.pipe';
import { EmployeeUpdateComponent } from './views/employees/employee-update/employee-update.component';
import { RemoveConfirmComponent } from './modals/remove-confirm/remove-confirm.component';
import { UserUpdateComponent } from './views/user/user-update/user-update.component';
import { CompanyComponent } from './views/company/company.component';
import { ProductsComponent } from './views/store/products/products.component';
import { ProductsUpdateComponent } from './views/store/products/products-update/products-update.component';
import { CompanyUpdateComponent } from './views/company/company-update/company-update.component';
import { FaqComponent } from './views/faq/faq.component';
import { FaqUpdateComponent } from './views/faq/faq-update/faq-update.component';
import { MissionsComponent } from './views/missions/missions.component';
import { MissionsUpdateComponent } from './views/missions/missions-update/missions-update.component';
import { MissionsRealComponent } from './views/missions-real/missions-real.component';
import { MissionsRealUpdateComponent } from './views/missions-real/missions-real-update/missions-real-update.component';
import { TestValidatorDirective } from './directives/test-validator.directive';
import { MinValidatorDirective,MaxValidatorDirective } from './directives/validators.directive';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot({
      preventDuplicates: true,
      newestOnTop: true,
      closeButton: false,
      progressBar: true,
      enableHtml: true,
      // hideMethod: "slideDown",
      positionClass: "toast-top-center",
      maxOpened: 1,
      autoDismiss: true,
      timeOut: 5000
    }),
    NgxMaskModule.forRoot({})
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    TestComponent,
    UserComponent,
    EmployeesComponent,
    UserUpdateComponent,
    CompanyComponent,
    DateTimestampPipe,
    EmployeeUpdateComponent,
    RemoveConfirmComponent,
    ProductsComponent,
    ProductsUpdateComponent,
    CompanyUpdateComponent,
    FaqComponent,
    FaqUpdateComponent,
    MissionsComponent,
    MissionsUpdateComponent,
    MissionsRealComponent,
    MissionsRealUpdateComponent,
    MinValidatorDirective,
    MaxValidatorDirective
  ],
  entryComponents: [
    RemoveConfirmComponent
  ],
  providers: [
    AuthGuardService,
    CookieService,
    ApiService,
    UtilsService,
    UserService,
    UsersCommonService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Commonterceptor,
      multi: true
    }


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}